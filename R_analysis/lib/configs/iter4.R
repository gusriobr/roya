

treat_dataset <- function(df){
  df$logit_porc_last_year <- logit(df$porc_last_year)
  df[is.infinite(df$logit_porc_last_year),]$logit_porc_last_year <- -10
  return(df)
}

create_formulas <- function(){
  mformulas <- list()
  
  # mform <- list(name="simple", formula= ~1 + temp_mean + hum_mean + week)
  # mformulas[[length(mformulas)+1]] <- mform
  # mform <- list(name="lastSeason", formula= ~1 + temp_mean + hum_mean + week + porc_last_year)
  # mformulas[[length(mformulas)+1]] <- mform
  mform <- list(name="logit-lastS", formula= ~1 + temp_mean + hum_mean + week + logit_porc_last_year)
  mformulas[[length(mformulas)+1]] <- mform
  mform <- list(name="height-logit_lastS", formula= ~1 + temp_mean + hum_mean + week + altitud + logit_porc_last_year)
  mformulas[[length(mformulas)+1]] <- mform
  mform <- list(name="height-logitxlastS", formula= ~1 + temp_mean + hum_mean + week + altitud*logit_porc_last_year)
  mformulas[[length(mformulas)+1]] <- mform
  
  # fixed coefs
  for (i in 1:length(mformulas)){
    varnames <- labels(terms(mformulas[[i]]$formula))
    mformulas[[i]]$coef_names <- paste("beta[", 1:length(varnames),"]", sep="")
  }
  return(mformulas)
}

create_models <- function(mformula, data){
  model_defs <- list()
  model_defs[["negBinomial"]] <- negBinomial_model("negBinomial", mformula, model_file = "models/negBinomial")
  return (model_defs)
}


# model_defs[["poisson"]] <- poisson_model("poisson", mformula)
# model_defs[["binomial"]] <- binomial_model("binomial", mformula)
# model_defs[["negBinomial"]] <- negBinomial_model("negBinomial", mformula)
# zero-inflated models
# model_defs[["ziPoisson"]] <- zipoisson_model("ziPoisson", mformula)
# model_defs[["ziBinomial"]] <- ziBinomial_model("ziBinomial", mformula)
# model_defs[["ziNegBinomial"]] <- ziNegBinomial_model("ziNegBinomial", mformula)
