import os

DATA_LAKE_FOLDER = '/media/data/datalake/'


def get_base_folder():
    return os.path.dirname(os.path.dirname(__file__))


def get_resource_folder():
    return os.path.join(get_base_folder(), "resources")


def dresults(path):
    return os.path.realpath(os.path.join(get_base_folder(), "../results", path))


def dresource(path):
    return os.path.join(get_resource_folder(), path)


def dlake(path):
    return os.path.join(DATA_LAKE_FOLDER, path)


if __name__ == '__main__':
    print("__" + dresults("xgboost"))
