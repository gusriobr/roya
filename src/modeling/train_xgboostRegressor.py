import os

import numpy as np
import pandas as pd
from joblib import dump
# from keras.losses import mean_squared_error, mean_absolute_error
from sklearn import preprocessing
from sklearn.feature_selection import RFECV
from sklearn.metrics import mean_squared_error, mean_absolute_error
from xgboost import XGBRegressor

# NOTE: Make sure that the outcome column is labeled 'target' in the data file
from data.datasets import load_dataset, load_dataset_count
from eval import cross_val_summary_simple, plot_cv_results
from modeling.training import create_training_folder


def train_perc_detected():
    base_cols_perc = ['temp_min', 'temp_max', 'temp_mean', 'temp_median', 'temp_std',
                      'prec_min', 'prec_max', 'prec_mean', 'prec_median', 'prec_std',
                      'hum_min', 'hum_max', 'hum_mean', 'hum_median', 'hum_std',
                      # 'wdir_min', 'wdir_max', 'wdir_mean', 'wdir_median', 'wdir_std',
                      'wspeed_min', 'wspeed_max', 'wspeed_mean', 'wspeed_median', 'wspeed_std',
                      'radiation_min', 'radiation_max', 'radiation_mean', 'radiation_median', 'radiation_std',
                      'prec_10d', 'prec_30d', 'prec_60d',
                      'temp_int_4_30',
                      'roya_event_skip_10d',  # 'roya_event_10d',
                      'prec_expected_30d', 'prec_exp_perc_30d',  # 'prec_exp_sgm_30d',
                      'prec_expected_60d', 'prec_exp_perc_60d',  # 'prec_exp_sgm_60d',
                      'dyear_sin', 'dyear_cos',
                      'roya_perc_event_skip_10d',
                      'altitud',
                      # 'perc_detected_10d_t1', 'perc_detected_10d_t3', 'perc_detected_10d_t10'
                      ]

    experiments = [
        ("base_cols", base_cols_perc),
        ("base_with_wind",
         base_cols_perc + ['wdir_min', 'wdir_max', 'wdir_mean', 'wdir_median', 'wdir_std']),
        ("base_with_autoR",
         base_cols_perc + ['perc_detected_10d_t1', 'perc_detected_10d_t3', 'perc_detected_10d_t10'])
    ]
    train_xgboost(experiments, 'perc_detected_10d', load_dataset, feature_selection=True)
    train_xgboost(experiments, 'perc_detected_10d', load_dataset, feature_selection=False)


def train_num_detected():
    base_cols_perc = ['temp_min', 'temp_max', 'temp_mean', 'temp_median', 'temp_std',
                      'prec_min', 'prec_max', 'prec_mean', 'prec_median', 'prec_std',
                      'hum_min', 'hum_max', 'hum_mean', 'hum_median', 'hum_std',
                      # 'wdir_min', 'wdir_max', 'wdir_mean', 'wdir_median', 'wdir_std',
                      'wspeed_min', 'wspeed_max', 'wspeed_mean', 'wspeed_median', 'wspeed_std',
                      'radiation_min', 'radiation_max', 'radiation_mean', 'radiation_median', 'radiation_std',
                      'prec_10d', 'prec_30d', 'prec_60d',
                      'temp_int_4_30',
                      'roya_event_skip_10d',  # 'roya_event_10d',
                      'prec_expected_30d', 'prec_exp_perc_30d',  # 'prec_exp_sgm_30d',
                      'prec_expected_60d', 'prec_exp_perc_60d',  # 'prec_exp_sgm_60d',
                      'dyear_sin', 'dyear_cos',
                      'roya_perc_event_skip_10d',
                      'altitud', 'n_um_10d',
                      # 'perc_detected_10d_t1', 'perc_detected_10d_t3', 'perc_detected_10d_t10'
                      ]

    experiments = [
        ("base_cols", base_cols_perc),
    ]
    train_xgboost(experiments, 'detected_10d', load_dataset_count, feature_selection=True)
    train_xgboost(experiments, 'detected_10d', load_dataset_count, feature_selection=False)


def train_xgboost(experiments, response_var, dataset_func, feature_selection=True):
    training_folder = create_training_folder('xgboost', response_var)
    f = open(os.path.join(training_folder, "metrics.txt"), "a+")

    df_results = None
    for exp in experiments:
        name, cols = exp
        f.write("============ {} ===============\n".format(name))

        if feature_selection:
            print("Training with set: {}.".format(name))
            (X_train, y_train), (X_test, y_test), (X_val, y_val) = dataset_func(response_var=response_var, cols=cols)

            X_train_test = np.vstack([X_train, X_test])
            y_train_test = np.hstack([y_train, y_test])

            model = XGBRegressor(learning_rate=0.1, max_depth=10, min_child_weight=16, n_estimators=100, nthread=1,
                                 objective="reg:squarederror", subsample=0.9000000000000001)

            scaler = preprocessing.MinMaxScaler().fit(X_train_test)
            selector = RFECV(model, step=1, cv=5, scoring="neg_mean_squared_error")
            selector.fit(scaler.transform(X_train_test), y_train_test)
            print(selector.support_)
            print(selector.ranking_)
            # model.fit(scaler.transform(X_train), y_train)
            selected_cols = X_train.columns[selector.support_].to_list()
            f.write("Selected features: {}\n".format(selected_cols))
            cols = selected_cols

        ## train model with selected columns
        f.write("Training regressor with set: \n{}\n.".format(cols))
        (X_train, y_train), (X_test, y_test), (X_val, y_val) = load_dataset(response_var=response_var,
                                                                            cols=cols)
        model = XGBRegressor(learning_rate=0.1, max_depth=10, min_child_weight=16, n_estimators=100, nthread=1,
                             objective="reg:squarederror", subsample=0.9000000000000001)

        scaler = preprocessing.MinMaxScaler().fit(X_train)
        X = scaler.transform(np.vstack([X_train, X_test]))
        y = np.hstack([y_train, y_test])

        df_metrics, model_map = cross_val_summary_simple([(name, model)], X, y, scoring='neg_mean_squared_error',
                                                         splits_kfold=4, ntimes=5)
        if df_results is None:
            df_results = df_metrics
        else:
            df_results = pd.concat([df_results, df_metrics])

        fitted_model = model_map[name][0]
        y_pred = fitted_model.predict(scaler.transform(X_test))
        y_pred_val = fitted_model.predict(scaler.transform(X_val))
        # save model
        dump(fitted_model, os.path.join(training_folder, "model_{}.joblib".format(name)))
        f.write("MSE/MAE sobre el conjunto de test: {:.5f}/{:.5f}\n".format(mean_squared_error(y_test, y_pred),
                                                                      mean_absolute_error(y_test, y_pred)))
        f.write("MSE/MAE sobre el conjunto de validacion: {:.5f}/{:.5f}\n".format(mean_squared_error(y_val, y_pred_val),
                                                                            mean_absolute_error(y_val, y_pred_val)))

    df_results.value.describe().to_csv(os.path.join(training_folder, "stats.txt"))
    df_results.to_excel(os.path.join(training_folder, "results.xlsx"))
    plot_cv_results(df_results, output_file=os.path.join(training_folder, "comparative.png"))


if __name__ == '__main__':
    train_perc_detected()
    train_num_detected()
