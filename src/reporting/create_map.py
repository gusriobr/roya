import sys
from datetime import datetime

import numpy as np

sys.path.append("../..")  # go to parent dir
# from customFunctions import *

from modeling.training import load_model, load_scaler

import pandas as pd
import geopandas as gp
from plotnine import *

PUBLISH_FOLDER = ""
PROJECT_RESOURCES = ""
DATALAKE_FOLDER = "";


def add_features(df):
    df["year"] = df["date"].dt.year
    df["month"] = df["date"].dt.month
    df['month_sin'] = np.sin(2 * np.pi * df['month'] / 12)
    df['month_cos'] = np.cos(2 * np.pi * df['month'] / 12)
    df["week"] = df["date"].dt.isocalendar().week
    df['week_sin'] = np.sin(2 * np.pi * df['week'] / 53)
    df['week_cos'] = np.cos(2 * np.pi * df['week'] / 53)
    df["dyear"] = df["date"].dt.dayofyear
    df['dyear_sin'] = np.sin(2 * np.pi * df['dyear'] / 365)
    df['dyear_cos'] = np.cos(2 * np.pi * df['dyear'] / 365)

    df["roya_perc_event_skip_10d"] = df['roya_event_skip_10d'] / (10 * 24 * 2)
    df["roya_perc_event_10d"] = df['roya_event_10d'] / (10 * 24 * 2)

    # get height for weather stations
    dfs = gp.read_file('/media/data/cartography/estaciones/Inforiego/ESTACIONES_INFORIEGO.shp')
    dfs["idest"] = dfs.IDPROVINCI.astype(str).str.pad(2, "left", '0') + dfs.IDESTACION.astype(str).str.pad(3, "left",
                                                                                                           '0')
    dfs["prov"] = dfs.IDPROVINCI.astype(str).str.pad(2, "left", '0')
    df = df.merge(dfs[["idest", "ALTITUD", "prov", "geometry", 'XPUBLICAS', 'YPUBLICAS']], on="idest")
    df.rename({"ALTITUD": "altitud", "IDPROVINCI": "prov"}, axis=1, inplace=True)

    # convert datatypes
    dt = df.dtypes
    cols = dt[dt == "float64"].T.keys().to_list()
    df = df.astype({x: "float32" for x in cols})

    return df


def predict(df):
    cols = ['temp_min', 'temp_max', 'temp_mean', 'temp_median', 'temp_std',
            'prec_min', 'prec_max', 'prec_mean', 'prec_median', 'prec_std',
            'hum_min', 'hum_max', 'hum_mean', 'hum_median', 'hum_std',
            # 'wdir_min', 'wdir_max', 'wdir_mean', 'wdir_median', 'wdir_std',
            'wspeed_min', 'wspeed_max', 'wspeed_mean', 'wspeed_median', 'wspeed_std',
            'radiation_min', 'radiation_max', 'radiation_mean', 'radiation_median', 'radiation_std',
            'prec_10d', 'prec_30d', 'prec_60d',
            'temp_int_4_30',
            'roya_event_skip_10d',  # 'roya_event_10d',
            'prec_expected_30d', 'prec_exp_perc_30d',  # 'prec_exp_sgm_30d',
            'prec_expected_60d', 'prec_exp_perc_60d',  # 'prec_exp_sgm_60d',
            'dyear_sin', 'dyear_cos',
            'roya_perc_event_skip_10d',
            'altitud',
            # 'perc_detected_10d_t1', 'perc_detected_10d_t3', 'perc_detected_10d_t10'
            ]

    models = [
        ("xgb",
         cols,
         load_model('xgboost/20201222_090208_perc_detected_10d_noAutoR'),
         load_scaler('xgboost/20201222_090208_perc_detected_10d_noAutoR'),
         )
    ]

    # predict
    for r in models:
        name, cols, model, scaler = r
        df["pred_" + name] = model.predict(scaler.transform(df[cols]))

    # lower censoring
    df.loc[df["pred_xgb"] < 0, "pred_xgb"] = 0


def generate_map_plot(df, day):
    df_provs = gp.read_file('/media/data/cartography/provincias/au.prov_cyl_recintos.shp')
    # filter dataset
    filtered_df = df[df.date == day]
    p = (ggplot()
         + geom_map(df_provs, fill="white")
         + geom_map(aes(fill='pred_xgb'), data=filtered_df, size=7, alpha=0.5)
         + geom_text(aes('XPUBLICAS', 'YPUBLICAS', label='idest'),
                     data=filtered_df, size=10, alpha=0.8, ha='left')
         + labs(title="Predicción roya amarilla")
         + annotate('text', y=4450000, x=500000, label='{:%Y-%b-%d}'.format(day), size=25)
         + scale_fill_continuous('viridis', limits=[0, 0.5])
         + theme(figure_size=(13, 10))
         )
    return p


def create_daily_map(dataset: str, day_str: str):
    day = datetime.strptime(day_str, "%Y-%m-%d")
    df = pd.read_parquet(dataset)
    df = add_features(df)
    predict(df)
    plot = generate_map_plot(df, day)
    plot.save("/tmp/salida_{:%Y%m%d}.png".format(day))


if __name__ == '__main__':
    dts_file = '/media/data/climate/inforiego_2014-2020_daily.pkt'
    create_daily_map(dts_file, datetime(2020, 5, 10))
