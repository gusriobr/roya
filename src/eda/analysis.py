# !/usr/bin/env python
# -*- coding: utf-8 -*-
import datetime
import os

import numpy as np
import pandas as pd
import sklearn.ensemble as skem
import sklearn.feature_selection as skselect
import sklearn.linear_model as skmod
import sklearn.metrics as skmet
import sklearn.svm as sksvm
from imblearn.under_sampling import RandomUnderSampler
from matplotlib import pyplot as plt
from sklearn.model_selection import StratifiedKFold, train_test_split

import recipes.evaluation_classification as evalc
import recipes.exploratory_analysis as eda
import recipes.feature_reduction as freduct
import recipes.feature_selection as fselect
import recipes.hypothesis as hyp
import recipes.plot as rg
import recipes.projection_evaluation as prj
from recipes.exploratory_analysis import descriptive_statistics, print_column_datatypes


def resource_folder():
    return "../resources/processed"


def output_folder():
    return "../report"


def config_printing():
    np.set_printoptions(linewidth=500)
    np.set_printoptions(precision=4)
    pd.set_option('display.width', 900)
    pd.set_option('precision', 4)
    # plt.rcParams["figure.figsize"] = 15, 15


def read_data():
    filename = os.path.join(resource_folder(), "full_data.h5")
    df = pd.read_hdf(filename)

    # data cleaning and completion
    # replace white spaces
    df.columns = [x.strip().replace("+", "").replace(" ", "_") for x in df.columns]

    # filter columns related to weather events
    weather_columns = df.columns.tolist()
    weather_columns = [x for x in weather_columns if
                       ("EVENT" in x) or ("TEMP_" in x) or ("HUM_" in x) or ("PREC_" in x)]
    selected_cols = ["IDEST", "ZV", "DATE", "DETECTADO"] + weather_columns
    df = df[selected_cols]

    # treat DETECTADO as dicotomic variable
    df.loc[df["DETECTADO"] > 0, ["DETECTADO"]] = 1

    # df.drop(["Temperatura"], axis=1, inplace=True)
    # df.drop([y_var], axis=1, inplace = True)
    # ordenar columnas por nombre
    df = df.reindex_axis(sorted(df.columns), axis=1)
    df.columns = [x.lower() for x in df.columns]
    return df


def treat_missing_data(df, categorical_cols):
    # 1 - Mark wrong values as NAN example
    # df[[1, 2, 3, 4, 5]] = df[[1, 2, 3, 4, 5]].replace(0, numpy.NaN)
    # 2 - Drop missing values
    # 3 - Imputer
    # replace missing data using column average value
    df.fillna(df.mean(), inplace=True)
    return df


if __name__ == '__main__':

    exploratory_analysis = False
    data_visualization = False
    analyze_normality = False
    under_sampling = True
    normalize_data = True
    anova_hypothesis_test = False
    feature_selection = False
    feature_reduction = False
    train_models = True
    model_evaluation = True

    # config_printing()

    # Data analysis
    df = read_data()
    y_var = "detectado"

    print_column_datatypes(df)
    descriptive_statistics(df)
    # df = treat_missing_data(df, [y_var])
    # df = eda.normalize_data_sklearn(df, [y_var])
    # df = normalize_data(df, ["Gender"])
    # df = standardize_data(df, ["Gender"])

    if exploratory_analysis:
        descriptive_statistics(df)
        # eda.analyze_class_skewness(df, [y_var])
        # eda.analyze_distribution(df, layout=(6, 6))
        # eda.plot_profile(df, excluded_columns=[y_var])
        # eda.plot_parallel_coordinates(df, categorical_column=y_var)
        # outlier detection
        corr_matrix = eda.analyze_correlation(df, corr_method="pearson",
                                              output_file=os.path.join(output_folder(), "corr_pearson.png"))
        print(eda.sort_correlations(corr_matrix))
        corr_matrix = eda.analyze_correlation(df, corr_method="spearman",
                                              output_file=os.path.join(output_folder(), "corr_spearman.png"))
        print(eda.sort_correlations(corr_matrix))

        # calculate correlations between variables an DETECTADO column for each ZV

        for zv in df["ZV"].unique():
            corr_matrix = eda.analyze_correlation(df[df["ZV"] == zv], corr_method="pearson",
                                                  output_file=os.path.join(output_folder(),
                                                                           "corr_pearson_{}.png".format(zv)))

        # correlation matrix per ZV
        # zvs =
    if data_visualization:
        # response variable classes balance
        df["detectado"].hist()
        plt.savefig(os.path.join(output_folder(), "class_histogram.png"))
        # set date as index

        year_lst = [2015, 2016, 2017, 2018]
        df_norm = eda.normalize_data_sklearn(df, excluded_columns=["idest", "zv", "date"])
        lst_stats = df["idest"].unique()
        df_norm.set_index(keys=["date"], inplace=True)

        column_selection = []
        # column_selection.append(["full", [x for x in df.columns if ("detectado" == x) or ("event_" in x)]])
        # column_selection.append(["decade", [x for x in df.columns if ("detectado" == x) or ("dec_event_" in x)]])
        column_selection.append(["clima", [x for x in df.columns if
                                           ("detectado" == x) or ("temp_" in x) or ("hum_" in x) or ("prec_" in x)]])

        for data in column_selection:
            tag = data[0]
            selected_cols = data[1]
            for year in year_lst:
                output_file = os.path.join(output_folder(), "datavis_{}_{}.png".format(year, tag))

                start = datetime.datetime(year, 1, 1)
                end = datetime.datetime(year, 12, 31)

                df_filtered = df_norm[(df_norm.index > start) & (df_norm.index < end)]

                plt.figure()
                fig, axes = plt.subplots(nrows=len(lst_stats), ncols=1, figsize=(8, 25))
                plt.subplots_adjust(hspace=2.5)

                lines = []
                labels = []
                for idx, st in enumerate(lst_stats):
                    df_st = df_filtered[(df_filtered["idest"] == st)][selected_cols]
                    line = df_st.plot(ax=axes[idx], legend=False)
                    lines.append(line)
                    axes[idx].set_title(st)

                plt.tight_layout()
                plt.subplots_adjust(right=0.8)
                plt.gcf().axes[0].legend(bbox_to_anchor=(1.04, 1), loc="upper left", prop={"size": 6})
                plt.savefig(output_file)

    if analyze_normality:
        eda.plot_normal_probability(df, excluded_columns=[y_var])
        eda.profile_plot(df, excluded_columns=[y_var])

    y = df[y_var]
    exclude_cols = [y_var, "date", "zv", "idest"]
    exclude_cols.extend([x for x in df.columns if ("temp_" in x) or ("hum_" in x) or ("prec_" in x)])
    X = df.drop(exclude_cols, axis=1)
    if normalize_data:
        X = eda.normalize_data_sklearn(X)
        X = eda.standardize_data_sklearn(X)

    if under_sampling:
        cc = RandomUnderSampler(ratio="majority")
        X_sampled, y_sampled = cc.fit_sample(X, y)
        X = pd.DataFrame(X_sampled, columns=X.columns)
        y = pd.Series(y_sampled)


    if anova_hypothesis_test:
        print("============= ANALISIS DE VARIANZA ======================")
        output_file = os.path.join(resource_folder(), "anova.xlsx")
        anova_writer = pd.ExcelWriter(output_file, engine='openpyxl')

        hyp.anova_plot_group_means(df, categorical_variable=y_var)
        # hyp.anova_test(df, y_var)
        print(">>> Hipotesis de normalidad")
        norm_test = hyp.normality_test(df, excluded_columns=y_var)
        norm_test.to_excel(anova_writer, sheet_name="normalidad")
        print(norm_test)
        print(">>> Hipotesis de homocedasticidad.")
        barl_test = hyp.homocedas_barlett(df, excluded_columns=y_var)
        # barl_test.to_excel(anova_writer, sheet_name="homocedasticidad-Barlett")
        print(barl_test)

        levene_test = hyp.homocedas_barlett(df, excluded_columns=y_var)
        # levene_test.to_excel(anova_writer, sheet_name="homocedasticidad-Levene")
        print(levene_test)

        hyp.manova_test(df, y_var)

    if feature_selection:
        fselect.univariable_selection(X, y, 6)
        fselect.univariable_selection(X, y, 6, score_func=skselect.f_classif)
        fselect.RFE(X, y, 6)
        fselect.tree_classifier(X, y, 6)

    if feature_reduction:
        projected_features, explained_variance = freduct.PCA(X_standardized, y, explained_variance=0.85)
        df_pca_corr, relevant = prj.pca_correlations(projected_features, X_standardized)
        # delete
        projected_features["y"] = y
        prj.save_pca_analysis(projected_features, df_pca_corr)

        print(">>> Relevant Principal Components relations: ")
        print(relevant)

        freduct.pca_show_variance_rate(X)
        # plot two main components
        data1 = projected_features[y == "Vacío"]
        data2 = projected_features[y == "Atmósferica"]
        # convert to numpy arrays
        if data1.shape[1] == 2:  # at least 2 columns + categorical variable
            data1 = data1[["col_0"]].values.T
            data2 = data2[["col_0"]].values.T
        else:
            data1 = data1[["col_0", "col_1"]].values.T
            data2 = data2[["col_0", "col_1"]].values.T
        rg.bivariate_kde(data1, "Vacío", data2, "Atmosférica", plot_title="PCA reduction")

        projected_features, explained_variance = freduct.LDA(X_standardized, y, explained_variance=0.85, n_components=2)
        # plot two main components
        data1 = projected_features[y == "Vacío"]
        data2 = projected_features[y == "Atmósferica"]
        # convert to numpy arrays
        if data1.shape[1] == 1:
            data1 = data1[["col_0"]].values.T
            data2 = data2[["col_0"]].values.T
        else:
            data1 = data1[["col_0", "col_1"]].values.T
            data2 = data2[["col_0", "col_1"]].values.T

        # rg.bivariate_kde(data1, "Vacío", data2, "Atmosférica", plot_title="LDA reduction")

    # train model
    if train_models:
        # regression
        # kfold = skmodsel.KFold(n_splits=10, random_state=7)
        kfold = StratifiedKFold(n_splits=10)

        models = [["LogRg", skmod.LogisticRegression()],
                  # ["Lasso", skmod.Lasso()],
                  ["Ridge", skmod.RidgeClassifier()],
                  # ["PasAgr", skmod.PassiveAggressiveClassifier()],
                  # ["kMeans", KNeighborsClassifier()],
                  ["XTree", skem.ExtraTreesClassifier()],
                  ["SVM-L", sksvm.LinearSVC()],
                  # ["GausNB", GaussianNB()],
                  # ["SVC-l", sksvm.SVC(kernel='linear')],
                  # ["SVC-RBF", sksvm.SVC(kernel='rbf')]
                  ]
        scoring = "f1"

        print("============= SIN Reducción ======================")
        summary = evalc.cross_val_summary_simple(models, X, y, scoring=scoring, options={"title": "NO RED"})
        print("===========================================")
        # print("============= CON PCA ======================")
        # x_proyected, scores = freduct.PCA(X_standardized, y, explained_variance=0.85)
        # summary = evalc.cross_val_summary_simple(models, x_proyected, y, scoring=scoring, options={"title": "PCA"})
        # print("===========================================")
        # print("============= CON LDA ======================")
        # x_proyected, scores = freduct.LDA(X, y, explained_variance=0.85)
        # summary = evalc.cross_val_summary_simple(models, x_proyected, y, scoring=scoring, options={"title": "LDA"})
        # print("===========================================")

        if model_evaluation:
            print("============== MODEL EVALUATION ===================")

            model = skem.ExtraTreesClassifier()
            summary = evalc.cross_val_summary(models, X, y, metrics=["f1"])  # metrics=["accuracy", "roc_auc"])

            X_train, X_test, Y_train, y_true = train_test_split(X, y, test_size=0.3)
            model.fit(X_train, Y_train)
            y_predicted = model.predict(X_test)

            # y_true, y_predicted = evalc.train_model(models[0][1], X, y)

            cmtx = skmet.confusion_matrix(y_true, y_predicted)
            class_names = np.unique(y)
            evalc.plot_confusion_matrix(cmtx, classes=class_names, normalize=True)

            report = skmet.classification_report(y_true, y_predicted)
            print(report)

        plt.show()

    print("============== ANALYSIS FINISHED ===================")
