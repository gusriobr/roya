import os

import pandas as pd

import cfg
from data.extract_weather_features import prepare_expected_railfall_df, calculate_daily_data, \
    calculate_expected_rainfall_features, calculate_daily_roya_events, \
    calculate_expected_prec_features

BASE_FOLDER = "/media/data/climate"


def parse_csv():
    #     read input file
    mydateparser = lambda x: pd.datetime.strptime(x, "%d/%m/%y %H:%M:%S")
    input_file = os.path.join(BASE_FOLDER, 'inforiego_2014-2020.dsv')
    df_weather = pd.read_csv(input_file, sep=",", encoding="UTF-8",
                             dtype={'IDPROVINCIA': np.int8, 'IDESTACION': np.int8, 'YEAR': int,
                                    'HORAMIN': int, 'TEMPMEDIA': np.float32,
                                    'HUMEDADMEDIA': np.float32,
                                    'VELVIENTO': np.float32, 'DIRVIENTO': np.float32, 'RADIACION': np.float32,
                                    'PRECIPITACION': np.float32,
                                    'TEMPMEDIACAJA': np.float32}, parse_dates=['FECHACOMPLETA'],
                             date_parser=mydateparser)
    df_weather["idest"] = df_weather["IDPROVINCIA"].astype(str).str.pad(2, "left", fillchar="0") + df_weather[
        "IDESTACION"].astype(
        str).str.pad(3, "left", fillchar="0")
    df_weather.drop("IDESTACION", axis=1, inplace=True)

    # convert columns
    col_order = ['IDPROVINCIA', 'idest', 'FECHACOMPLETA', 'YEAR', 'TEMPMEDIA',
                 'PRECIPITACION', 'HUMEDADMEDIA', 'DIRVIENTO', 'VELVIENTO', 'RADIACION']
    column_names = ['prov', 'idest', 'date', 'year', 'temp', 'prec', 'hum', 'wdir', 'wspeed', 'radiation']
    # sort columns and rename them
    df_weather = df_weather[col_order]
    df_weather.columns = column_names
    df_weather.to_parquet("D:/climate/inforiego_2014-2020.pkt")


if __name__ == '__main__':

    input_file_name = 'weather/data/inforiego_hourly_{}.pq'
    output_file_name = 'sativum/weather/inforiego_daily_data_{}.pq'

    for year in range(2015, 2016):  # 2021
        input_file = cfg.dlake(input_file_name.format(year))
        output_file = cfg.dlake(output_file_name.format(year))

        df_hourly = pd.read_parquet(input_file)

        df_daily = calculate_daily_data(df_hourly, debug=False)
        print("Daily data calculated!")

        df_daily.sort_values(["idest", "date"], inplace=True)
        df_daily.to_parquet(output_file)

        df_daily = calculate_expected_rainfall_features(df_daily)
        print("Expected rainfall features calculated!")

        df_daily.sort_values(["idest", "date"], inplace=True)
        df_daily.to_parquet(output_file)

        df_daily_roya = calculate_daily_roya_events(df_hourly)
        df_daily = df_daily.reset_index(0).set_index(["idest", "date"]) \
            .join(df_daily_roya, how="left") \
            .drop("idest", axis=1)

        df_daily = calculate_expected_prec_features(df_daily)

        # append timeserie features
        df_ts_features = pd.read_parquet(cfg.dlake('/sativum/weather/daily_weather_features_{}.pq'.format(year)))
        df_ts_features = df_ts_features.reset_index(0).set_index(["idest", "date"])
        df_daily = df_daily.reset_index(0).set_index(["idest", "date"]) \
            .join(df_daily_roya, how="left") \
            .drop("idest", axis=1)

        df_daily.sort_values(["idest", "date"], inplace=True)
        df_daily.to_parquet(output_file)
        print("Daily roya events calculated calculated!")

        # df_daily.to_excel("d:/tmp/salida.xlsx")
        print("Process successfully finished!")
