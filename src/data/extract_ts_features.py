import pandas as pd
from tsfresh import extract_features
from tsfresh.utilities.dataframe_functions import impute
from tsfresh.utilities.dataframe_functions import roll_time_series

import cfg

""""
Transforms dataset into a format that can be use to extract timeseries features usin tsfresh library

Takes as input the weather hourly station data, and calculates ts features for different variables 
creating and expanded parquet file. 
The weather data is  partitioned by year, to keep continuity, each year file is filled with previous month days
 
"""

fc_params = {
    'median': None,
    'mean': None,
    'standard_deviation': None,
    'maximum': None,
    'minimum': None,
    'skewness': None,
    'kurtosis': None,
    'agg_autocorrelation': [{'f_agg': 'mean', 'maxlag': 40}],
    'fourier_entropy': [{'bins': 5}, {'bins': 15}, {'bins': 50}],
    'number_peaks': [{'n': 5}, {'n': 15}, {'n': 50}],
    'agg_linear_trend': [
        {'attr': 'slope', 'chunk_len': 5, 'f_agg': 'mean'},
        {'attr': 'slope', 'chunk_len': 15, 'f_agg': 'mean'},
        {'attr': 'slope', 'chunk_len': 50, 'f_agg': 'mean'},
    ],
    'longest_strike_above_mean': None
}


def calculate_ts_features(df, window_days=10):
    # remove missing values due to 60d-prec extimation
    df = df[~df.isnull().any(axis=1)]

    # df = df[df.idest.isin(["05001", "47001"])]
    # df = df[df.date.between("2015-1-1", "2015-12-31")]

    # numeric_cols = ['temp_median']  # , 'hum_median', 'prec_median']
    numeric_cols = df.dtypes[df.dtypes == "float32"].index.to_list()
    numeric_cols = ["temp"]
    df_rolled = roll_time_series(df[["idest", "date"] + numeric_cols], column_id="idest", column_sort="date",
                                 min_timeshift=24 * window_days, max_timeshift=24 * window_days)

    # df_rolled.reset_index().to_feather(dresource('dataset/2015_2019_daily_v2_train-test_rolled.pck'))

    df_features = extract_features(df_rolled.drop("idest", axis=1), column_id="id", column_sort="date",
                                   default_fc_parameters=fc_params)
    df_features = impute(df_features)

    df_features.index.set_names(["idest", "date"], inplace=True)
    df.set_index(["idest", "date"], inplace=True)

    # keep only correlated features
    # y = df_features.join(df["perc_detected_10d"])["perc_detected_10d"]
    # df_features = select_features(df_features, y)
    renamed_cols = {}
    columns = [c for c in df_features.columns if "__" in c]
    for col in columns:
        c = col.replace("__", "_")
        if "minimum" in c:
            c = c.replace("minimum", "min")
        elif "maximum" in c:
            c = c.replace("maximum", "max")
        elif "standard_deviation" in c:
            c = c.replace("standard_deviation", "std")
        renamed_cols[col] = c

    df_features.rename(columns=renamed_cols, inplace=True)
    return df_features


if __name__ == '__main__':

    input_file_name = 'weather/data/inforiego_hourly_{}.pq'
    output_file_name = 'sativum/weather/inforiego_hourly_{}.pq'

    for year in range(2015, 2016):  # 2021
        # read file
        f_name = input_file_name.format(year)
        df = pd.read_parquet(cfg.dlake(f_name))
        df = df[(df.date.dt.month == 1)]

        # concatenate previous year december days
        df2 = pd.read_parquet(cfg.dlake(input_file_name.format(year - 1)))
        df2 = df2[(df2.date.dt.month == 12) & (df2.date.dt.year == year - 1)]
        df = pd.concat([df, df2])
        df.sort_values(["idest", "date"], inplace=True)

        # calculate features
        df_features = calculate_ts_features(df, window_days=2)
        df_features.to_parquet(cfg.dlake(output_file_name.format(year)))

    # # load original data
    # ds_path = dresource('dataset/2015_2019_daily_v2_train-test.pck')
    # df = pd.read_parquet(ds_path)
    #
    # # remove missing values due to 60d-prec extimation
    # df = df[~df.isnull().any(axis=1)]
    # # df = df[df.idest.isin(["05001", "47001"])]
    # df = df[df.date.between("2015-1-1", "2015-12-31")]
    #
    # numeric_cols = ['temp_median']  # , 'hum_median', 'prec_median']
    # df_rolled = roll_time_series(df[["idest", "date"] + numeric_cols], column_id="idest", column_sort="date",
    #                              min_timeshift=10, max_timeshift=10)
    #
    # # df_rolled.reset_index().to_feather(dresource('dataset/2015_2019_daily_v2_train-test_rolled.pck'))
    #
    # df_features = extract_features(df_rolled.drop("idest", axis=1), column_id="id", column_sort="date")
    # df_features = impute(df_features)
    #
    # df_features.index.set_names(["idest", "date"], inplace=True)
    # df.set_index(["idest", "date"], inplace=True)
    #
    # y = df_features.join(df["perc_detected_10d"])["perc_detected_10d"]
    #
    # features_filtered = select_features(df_features, y)
    # features_filtered.to_excel("/tmp/salida_features.xlsx")
    # print(df.shape)
    # print(df_rolled.shape)
    # print(df_features.shape)
    # print(features_filtered.shape)
