import os
from datetime import datetime
from pathlib import Path

import numpy as np
import pandas as pd
from sklearn.model_selection import train_test_split

BASE_FOLDER = Path(os.path.abspath(__file__)).parent.parent.parent


def res(path):
    return os.path.join(BASE_FOLDER, path)


def tstmp():
    return datetime.now().strftime("%Y%m%d_%H%M%S")


def load_dataset(response_var="perc_detected_10d", version="v2", cols=None, missing_values=False,
                 fill_missing_with=None):
    df = pd.read_parquet(
        os.path.join(BASE_FOLDER, "resources/dataset/2015_2019_daily_{}_train-test.pck".format(version)))
    df_val = pd.read_parquet(
        os.path.join(BASE_FOLDER, "resources/dataset/2015_2019_daily_{}_validation.pck".format(version)))
    if not missing_values:
        df = df[df["n_um_10d"] > 0]
        df_val = df_val[df_val["n_um_10d"] > 0]
    else:
        if fill_missing_with:
            df.loc[df["n_um_10d"] == 0, df.columns] = fill_missing_with
            df_val.loc[df_val["n_um_10d"] == 0, df_val.columns] = fill_missing_with

    y = df[response_var]
    y_val = df_val[response_var]
    if cols:
        X = df[cols]
        X_val = df_val[cols]
    else:
        X = df.drop(response_var, axis=1)
        X_val = df_val.drop(response_var, axis=1)

    X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.25, random_state=23)

    return (X_train, y_train), (X_test, y_test), (X_val, y_val)


def load_dataset_count(response_var="detected_10d", version="v2", cols=None, missing_values=False,
                       fill_missing_with=None):
    df = pd.read_parquet(
        os.path.join(BASE_FOLDER, "resources/dataset/2015_2019_daily_{}_train-test.pck".format(version)))
    df_val = pd.read_parquet(
        os.path.join(BASE_FOLDER, "resources/dataset/2015_2019_daily_{}_validation.pck".format(version)))

    if not missing_values:
        df = df[df["n_um_10d"] > 0]
        df_val = df_val[df_val["n_um_10d"] > 0]
    else:
        if fill_missing_with:
            df.loc[df["n_um_10d"] == 0, df.columns] = fill_missing_with
            df_val.loc[df_val["n_um_10d"] == 0, df_val.columns] = fill_missing_with

    y = df[response_var]
    y_val = df_val[response_var]
    if cols:
        X = df[cols]
        X_val = df_val[cols]
    else:
        X = df.drop(response_var, axis=1)
        X_val = df_val.drop(response_var, axis=1)

    X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.25, random_state=23)

    return (X_train, y_train), (X_test, y_test), (X_val, y_val)


def vars_fromdf(df, response_var, cols):
    y = df[response_var]
    if cols:
        X = df[cols]
    else:
        X = df.drop(response_var, axis=1)
    return X, y


def load_ts_dataset(sequence_length, response_var="perc_detected_10d", version="v2", cols=None, missing_values=False,
                    fill_missing_with=None):
    """
    Creates time series datasets using sliding windows technique to transform panel data to tabular data partitioning
    the dataset by weather station
    :param sequence_length:
    :param response_var:
    :param cols:
    :param missing_values:
    :param fill_missing_with:
    :return:
    """
    df = pd.read_parquet(os.path.join(BASE_FOLDER, "resources/dataset/2015_2019_daily_{}_train-test.pck".format(version)))
    df_val = pd.read_parquet(os.path.join(BASE_FOLDER, "resources/dataset/2015_2019_daily_{}_validation.pck".format(version)))
    ws_list = df.reset_index().idest.unique()

    df = df.sort_values(by=["idest", "date"])
    df_val = df_val.sort_values(by=["idest", "date"])

    if not missing_values:
        df = df[df["n_um_10d"] > 0]
        df_val = df_val[df_val["n_um_10d"] > 0]
    else:
        if fill_missing_with:
            df.loc[df["n_um_10d"] == 0, df.columns] = fill_missing_with
            df_val.loc[df_val["n_um_10d"] == 0, df_val.columns] = fill_missing_with

    X = None
    y = None
    for ws in ws_list:
        Xt, yt = vars_fromdf(df[df.idest == ws], response_var, cols)
        X_valt, y_valt = vars_fromdf(df_val[df_val.idest == ws], response_var, cols)

        Xts, yts = sliding_window(Xt.values, yt, sequence_length)
        X_valts, y_valts = sliding_window(X_valt.values, y_valt, sequence_length)

        if X is None:
            X = Xts
            y = yts
            X_val = X_valts
            y_val = y_valts
        else:
            X = np.vstack([X, Xts])
            y = np.hstack([y, yts])
            X_val = np.vstack([X_val, X_valts])
            y_val = np.hstack([y_val, y_valts])

    X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.25, random_state=23)
    return (X_train, y_train), (X_test, y_test), (X_val, y_val)


def load_ts_multiinput(sequence_length, response_var="perc_detected_10d", cols=None, missing_values=False,
                       fill_missing_with=None):
    # coger la serie horaria, anotarla con las variables diarias añadiendo la hora como columna
    # calcular el dataset temporal
    # quedarnos con los que tienen hora = 0.00

    # crear dataset y quedarnos con los que son de las 0.00

    df = pd.read_parquet(os.path.join(BASE_FOLDER, "resources/dataset/2015_2019_daily_train-test.pck"))
    df_val = pd.read_parquet(os.path.join(BASE_FOLDER, "resources/dataset/2015_2019_daily_validation.pck"))
    df = df.sort_values(by=["idest", "date"])
    df_val = df_val.sort_values(by=["idest", "date"])

    daily_cols = [
        'date', 'event_tot', 'event_tot_skip', 'temp_min', 'temp_mean', 'temp_median', 'temp_max',
        'temp_std', 'hum_min', 'hum_mean', 'hum_median', 'hum_max', 'hum_std',
        'prec_min', 'prec_mean', 'prec_median', 'prec_max', 'prec_std',
        'prec_acum_30', 'prec_acum_60', 'dec_event_per', 'dec_event_per_skip',
        'dec_event_tot', 'dec_event_tot_skip', 'prec_acum_exp_30',
        'prec_acum_exp_60', 'perc_dec_event_tot_skip', 'yday', 'week', 'month',
        'year', 'prec_acum_30_porc', 'prec_acum_60_porc',
        'prec_acum_30_porc_sgm', 'month_sin', 'month_cos', 'week_sin',
        'week_cos', 'dyear', 'dyear_sin', 'dyear_cos', 'temp_mean_10d_min',
        'hum_mean_10d_min', 'prec_mean_10d_min', 'temp_mean_10d_mean',
        'hum_mean_10d_mean', 'prec_mean_10d_mean', 'temp_mean_10d_max',
        'hum_mean_10d_max', 'prec_mean_10d_max', 'detected_10d',
        'n_um_10d', 'altitud', 'altitud_est', 'n_um', 'detected',
        'perc_detected', 'perc_detected_10d', 'perc_detected_10d_t5',
        'perc_detected_10d_t10', 'perc_detected_10d_t15']

    y = df[response_var]
    y_val = df_val[response_var]
    if cols:
        X = df[cols]
        X_val = df_val[cols]
    else:
        X = df.drop(response_var, axis=1)
        X_val = df_val.drop(response_var, axis=1)

    X, y = sliding_window(X.values, y, sequence_length)
    X_val, y_val = sliding_window(X_val.values, y_val, sequence_length)

    X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.25, random_state=23)

    return (X_train, y_train), (X_test, y_test), (X_val, y_val)


def sliding_window(dataset, y, look_back=1):
    """
    convert an array of values into a dataset matrix
    :param dataset:
    :param look_back:
    :return:
    """
    dataX, dataY = [], []
    limit = look_back
    for i in range(len(dataset) - limit):
        a = dataset[i:(i + look_back), :]
        dataX.append(a)
        # dataY.append(dataset[i + look_back, 0])
    # return np.array(dataX), np.array(dataY)
    return np.array(dataX), y[0:-limit]


if __name__ == '__main__':
    load_ts_dataset(sequence_length=30, missing_values=True)
