import logging
import os

import numpy as np
import pandas as pd

from data.evaluators import EvaluatorsFactory

logging.basicConfig(filename='prepare_data.log', filemode='w', format='%(asctime)s - %(levelname)s - %(message)s',
                    level=logging.INFO)

DECADE_WSIZE = 10
EVENT_WSIZE = 8  # 2 records/hour > 4 hours


def evaluate_daily_events(station_id, df, index_list):
    """
    Iterate over station weather rows calculating the number of "valid periods" of consecutive events.
    Data is already sorted by date in ascending order
    :param df:
    :return:
    """
    last_date = None
    day_rows = []

    EvaluatorsFactory.reset()
    record_updaters = EvaluatorsFactory.get_record_updaters()
    hourly_evaluators = EvaluatorsFactory.get_hourly_evaluators()
    daily_evaluators = EvaluatorsFactory.get_daily_evaluators()

    for index in index_list:
        # print("Estacion: {} fecha: {}".format(df.loc[index].IDEST, df.loc[index].FECHACOMPLETA))
        current_date = df.loc[index].date.date()
        _add_data(record_updaters, df.loc[index], current_date)
        _update_hour_record(record_updaters, df, index)

        _add_data(hourly_evaluators, df.loc[index], current_date)

        if current_date != last_date:
            # new day, if there's and event in course, increment counters to register, the end of
            # the event will be register two in the next day, but hours will be counted just once.
            # evaluate daily events
            row = {"idest": station_id, "date": last_date}
            _evaluate_data(hourly_evaluators, row)

            _add_data(daily_evaluators, row, last_date)
            _evaluate_data(daily_evaluators, row)
            if last_date is not None:
                # if last_date is not None register counters
                day_rows.append(row)

            # reset counters
            last_date = current_date

    # register final rows
    row = {"idest": station_id, "date": current_date}
    _evaluate_data(hourly_evaluators, row)
    _add_data(daily_evaluators, row, current_date)
    _evaluate_data(daily_evaluators, row)
    day_rows.append(row)

    return day_rows


def _update_hour_record(evaluators, df, index):
    for ev in evaluators:
        new_fields = ev.evaluate()
        for k, v in new_fields.items():
            df.at[index, k] = v


def _add_data(evaluators, row, current_date):
    for ev in evaluators:
        ev.add_data(row, current_date)


def _evaluate_data(evaluators, row):
    for ev in evaluators:
        new_fields = ev.evaluate()
        row.update(new_fields)


def preprocess_weather_data(df, hd5_file):
    # create station sourrogate id
    df["IDEST"] = df.IDPROVINCIA.apply("{0:02d}".format) + df.IDESTACION.apply("{0:03d}".format)
    # fields to caclulate
    df["EVENT"] = 0
    df["EVENT_SKIP"] = 0

    # Filter and sort columns
    # df = df.drop(columns=['HORAMIN', 'ANO', 'TEMPMEDIACAJA', 'FECHA', 'DIA'])
    df = df.drop(['HORAMIN', 'ANO', 'TEMPMEDIACAJA', 'FECHA', 'DIA'], axis=1)
    col_order = ['IDPROVINCIA', 'IDESTACION', 'IDEST', 'date', 'EVENT', 'EVENT_SKIP', 'HUMEDADMEDIA',
                 'TEMPMEDIA', 'PRECIPITACION', 'VELVIENTO', 'DIRVIENTO', 'RADIACION']
    df = df[col_order]
    # df.to_hdf(hd5_file + "_tmp", "data")

    print("Sorting df values")
    df = df.sort_values(by=["IDEST", "date"])

    print("Storing values in hd5 database.")
    df.to_hdf(hd5_file, "data")


def process_weather_data(df, fout_name, fout_daily, sort_values=True):
    """
    Iterates over the full weather data calculating the number of interesting events that show up within a decade
    :return:
    """
    df.sort_values(by=["idest", "date"], inplace=True)
    lst_stations = df["idest"].unique()
    i = 0
    df_daily = None
    total_daily_data = []
    # lst_stations = ["05001"]
    for st in lst_stations:
        logging.info(">>>>>> Going over station: " + st)
        # iterate over weather stations calculating events
        df_filtered = df[df["idest"] == st]
        # iterate over weather records to evaluate events on 4-hour windows
        index_list = df_filtered.index.values.tolist()

        # we use df_filtered to get the indexes, but the modification is done on the main dataframe
        daily_data = evaluate_daily_events(st, df, index_list)
        total_daily_data.extend(daily_data)

        # partial storing
        # df.to_excel(fout_name)
        # df.to_hdf(fout_name, "data")
        if df_daily is None:
            df_daily = pd.DataFrame(daily_data)
        else:
            df_daily = df_daily.append(pd.DataFrame(daily_data))
            df_daily["date"] = pd.to_datetime(df_daily["date"])

        # df_daily.to_excel(fout_daily)
        df_daily.to_hdf(fout_daily, "data")

    return df_daily


def old_processing():
    # TEST DATA
    preprocess_data = False  # False -> read data from .h5 instead of using the xlsx original file
    # input_file = "../resources/weather/datos_climaticos_2015_2019.h5"
    input_file = "../../resources/weather/datos_clima_test.h5"
    # input_file = "../resources/weather/datos_climaticos_2020.csv"

    if preprocess_data:
        print(">>> Reading weather data from " + input_file)
        mydateparser = lambda x: pd.datetime.strptime(x, "%d/%m/%y %H:%M:%S")
        df_weather = pd.read_csv(input_file, sep=";", encoding="UTF-8",
                                 dtype={'IDPROVINCIA': int, 'IDESTACION': int, 'ANO': int, 'DIA': int,
                                        'HORAMIN': int, 'TEMPMEDIA': np.float16,
                                        'HUMEDADMEDIA': np.float16,
                                        'VELVIENTO': np.float16, 'DIRVIENTO': np.float16, 'RADIACION': np.float16,
                                        'PRECIPITACION': np.float16,
                                        'TEMPMEDIACAJA': np.float16}, parse_dates=['FECHA', 'date'],
                                 date_parser=mydateparser,
                                 decimal=",")

        print("Lectura datos csv completada.")

        hd5_file = input_file.replace("csv", "h5")

        print("Weather data stored in hd5 file.")
        preprocess_weather_data(df_weather, hd5_file)
        input_file = hd5_file

    # input_file = "../resources/datos_clima_test.h5"
    print(">>> Reading weather data from " + input_file)
    df_weather = pd.read_hdf(input_file, "data")
    # df_weather = df_weather[
    #     (df_weather['date'] > pd.Timestamp(2015, 12, 1)) &
    #     (df_weather['date'] < pd.Timestamp(2016, 3, 1)) &
    #     (df_weather["IDEST"] == '47007')].copy()

    folder, file_name = os.path.split(input_file)
    name, ext = os.path.splitext(file_name)
    fout_name = os.path.join(folder, "processed", name + "_out.h5")
    fout_daily_name = os.path.join(folder, "processed", name + "_daily.h5")

    # df_weather = process_weather_data(df, out = )
    df_daily = process_weather_data(df_weather, fout_name=fout_name, fout_daily=fout_daily_name)

    # testing purposes
    print("Writing completed, exporting to excel files")
    # df_weather.to_excel(os.path.join(folder, name + "_out.xlsx"))
    # df_daily.to_excel(os.path.join(folder, "processed", name + "_daily.xlsx"))
    df_daily.to_csv(os.path.join(folder, "processed", name + "_daily.csv"), sep=";")
    # df_weather.to_hdf(fout_name, "data")


if __name__ == '__main__':
    BASE_FOLDER = "/media/data/climate"
    # input_file = os.path.join('inforiego_2014-2020_hourly-2020_daily.pkt')
    input_file = os.path.join('inforiego_2014-2020_hourly_test.pkt')
    df_weather = pd.read_parquet(input_file)

    folder, file_name = os.path.split(input_file)
    name, ext = os.path.splitext(file_name)
    fout_name = os.path.join(folder, "processed", name + "_out.h5")
    fout_daily_name = os.path.join(folder, "processed", name + "_daily.h5")

    # df_weather = process_weather_data(df, out = )
    df_daily = process_weather_data(df_weather, fout_name=fout_name, fout_daily=fout_daily_name)

    print("Writing completed, exporting to excel files")
    # df_weather.to_excel(os.path.join(folder, name + "_out.xlsx"))
    # df_daily.to_excel(os.path.join(folder, "processed", name + "_daily.xlsx"))
    df_daily.to_csv(os.path.join(folder, "processed", name + "_daily.csv"), sep=";")
    # df_weather.to_hdf(fout_name, "data")
