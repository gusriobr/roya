import gc
import os

import numpy as np
import pandas as pd


def sigmoid(X):
    return 1 / (1 + np.exp(-X))


weather_var_types = {
    'dyear': 'int16',
    'week': 'int8',
    'month': 'int8',
    'year': 'int16',
    'roya_event_10d': 'int16',
    'roya_event_skip_10d': 'int16',
    'detected_10d': 'int16',
    'n_um_10d': 'int16',
    'detected': 'int16',
    'n_um': 'int16'}


def prepare_cummulative_data():
    """
    Acumulado de variables meterológicas a 10d
    :return:
    """
    df = pd.read_hdf("/tmp/temporal.hd5")
    df = df.set_index(["date"])
    df = df.sort_index()

    grouped_min = df[["idest", "temp_mean", "hum_mean", "prec_mean"]].groupby("idest").rolling('10d').min()
    grouped_min.columns = ["idest"] + [x + "_min_10d" for x in ["TEMPMEDIA", "HUMEDADMEDIA", "PRECIPITACION"]]
    grouped_mean = df[["idest", "temp_mean", "hum_mean", "prec_mean"]].groupby("idest").rolling('10d').mean()
    grouped_mean.columns = ["idest"] + [x + "_mean_10d" for x in ["TEMPMEDIA", "HUMEDADMEDIA", "PRECIPITACION"]]
    grouped_max = df[["idest", "TEMPMEDIA", "HUMEDADMEDIA", "PRECIPITACION"]].groupby("idest").rolling('10d').max()
    grouped_max.columns = ["idest"] + [x + "_max_10d" for x in ["TEMPMEDIA", "HUMEDADMEDIA", "PRECIPITACION"]]

    df = df.reset_index(0)
    df = df.set_index(["idest", "FECHACOMPLETA"])

    for group in [grouped_min, grouped_mean, grouped_max]:
        df = df.join(group.drop("idest", axis=1), how="inner")

    return df


def prepare_weather_data(df):
    df.reset_index(inplace=True)
    df = df[df.date.dt.year.between(2015, 2019, inclusive=True)]
    # df["date"] = pd.to_datetime(df["date"], format='%Y-%m-%d')
    df["year"] = df["date"].dt.year
    df["month"] = df["date"].dt.month
    df['month_sin'] = np.sin(2 * np.pi * df['month'] / 12)
    df['month_cos'] = np.cos(2 * np.pi * df['month'] / 12)
    df["week"] = df["date"].dt.week  # isocalendar().week
    df['week_sin'] = np.sin(2 * np.pi * df['week'] / 53)
    df['week_cos'] = np.cos(2 * np.pi * df['week'] / 53)
    df["dyear"] = df["date"].dt.dayofyear
    df['dyear_sin'] = np.sin(2 * np.pi * df['dyear'] / 365)
    df['dyear_cos'] = np.cos(2 * np.pi * df['dyear'] / 365)

    df["roya_perc_event_skip_10d"] = df['roya_event_skip_10d'] / (10 * 24 * 2)
    df["roya_perc_event_10d"] = df['roya_event_10d'] / (10 * 24 * 2)

    # eliminar valores con NA (90 lineas de  87.000)
    df = df.dropna(axis=0)
    df.reset_index(inplace=True)
    df.columns = [x.lower() for x in df.columns.tolist()]

    return df


def prepare_parcel_data():
    # lectura caracterización recintos
    df_recintos = pd.read_csv("../../resources/caracterizacion_recintos.csv", sep=';')
    df_recintos.drop("ANIO", axis=1, inplace=True)
    df_recintos.dropna(axis=0, inplace=True)
    # df_recintos.astype(new_types)
    df_recintos.rename(columns={'PROVINCIA': 'COD_PROVINCIA'}, inplace=True)
    # df_recintos = df_recintos.astype()

    # lectura datos de campo
    df_datos_campo = pd.read_csv("../../resources/datos_campo_bruto_2015-2019.csv", sep=";")
    # df_recintos = df_recintos.astype()
    df_datos_campo["FECHA_INSPECCION"] = pd.to_datetime(df_datos_campo["FECHA_INSPECCION"], format='%d/%m/%Y')

    # merge por referencia de recinto
    df_result = df_datos_campo.merge(df_recintos, how="inner", left_on="REFREC", right_on="REFREC")
    print("Field data before merging: {}".format(df_datos_campo.shape[0]))
    print("Field data after merging: {}".format(df_result.shape[0]))
    assert df_result[df_result["IDEST"].isnull()].shape[0] == 0, "Some data don't have a weather station assigned!!"

    # liberar memoria
    del [[df_recintos, df_datos_campo]]
    gc.collect()

    new_types = {'ANO': 'int16',
                 'ASINTOMATICO': 'int16',
                 'COD MUNI': 'int32',
                 'COMARCA': 'object',
                 'DETECTADO': 'object',
                 'DMUNICIPIO': 'object',
                 'D_PLAGA': 'object',
                 'D_PRODUCTO': 'object',
                 'ESTADOFENOL': 'object',
                 'ESTADOFENOL.1': 'object',
                 'FECHA_INSPECCION': 'object',
                 'INCI_T0': 'int16',
                 'INCI_T1': 'int16',
                 'INCI_T2': 'int16',
                 'INCI_T3': 'int16',
                 'INCI_T4': 'int16',
                 'INCI_T5': 'int16',
                 'INICIAL': 'int16',
                 'INSPECTOR': 'object',
                 'N_UM': 'int16',
                 'N_VISITA': 'int16',
                 'PROBLEMA': 'int16',
                 'PROVINCIA': 'object',
                 'REFREC': 'object',
                 'RIESGO': 'int16',
                 'SAC': 'object',
                 #              }
                 # new_types = {
                 'AGREGADO': 'int16',
                 'ALTITUD': 'float32',
                 'ALTITUD_EST': 'int16',
                 'ALTITUD_EST_100': 'float32',
                 'DIST_IDEST': 'float32',
                 'DIST_IDEST_100': 'float32',
                 'IDEST': 'int32',
                 'IDEST_100': 'int32',
                 'MUNICIPIO': 'int16',
                 'PARCELA': 'int16',
                 'POLIGONO': 'int16',
                 'COD_PROVINCIA': 'int16',
                 'RECINTO': 'int16',
                 'REFREC': 'object',
                 'USO_SIGPAC': 'object',
                 'ZONA': 'int16'}

    # mark field data without weather station as station 999
    df_result.loc[df_result["IDEST"].isna(), "IDEST"] = 999
    df_result.drop(["Unnamed: 26"], axis=1, inplace=True)
    df_result = df_result.astype(new_types)
    df_result["date"] = pd.to_datetime(df_result["FECHA_INSPECCION"], format='%Y-%m-%d')
    df_result.columns = [x.lower() for x in df_result.columns.tolist()]

    df_result["detected"] = df_result["n_um"] - df_result["asintomatico"]
    # seleccionar columnas
    df_result = df_result[
        ['idest', 'cod muni', 'date', 'n_um', 'detected', 'altitud', 'dist_idest', 'altitud_est',
         'idest_100', 'dist_idest_100', 'altitud_est_100']]
    """
    Hasta aquí tenemos cada inspeccion anotada con la estación meteorológica que le corresponde
    ahora hay que crear un dataframe con cada día, y para cada día anotar:
    - estacíon meteo
    - numero de muestras tomadas y numero de detecciones en el periodo
    """

    # creamos dataframe con producto cartesian de dias x estaciones
    df_days = pd.DataFrame(
        {'date': pd.date_range(start=pd.Timestamp('2015-01-01'), end=pd.Timestamp('2019-12-31'), freq='D')})
    df_days["date"] = pd.to_datetime(df_days["date"], format='%Y-%m-%d')
    df_days["key"] = 0
    df_est = pd.DataFrame(data=df_result["idest"].unique(), columns=["idest"])
    df_est["key"] = 0
    df_days = df_days.merge(df_est, how='outer').drop(columns=["key"])
    # df_days = df_days.set_index(["idest", "date"]).sort_index()

    # df_result = df_result.set_index(["idest", "date"]).sort_index()
    # df_days = df_days.join(df_result)

    # cruzamos los registros- estacion-fecha con datos de campo por estacion-fecha_inspeccion
    df = df_days.merge(df_result, how="left", left_on=["idest", "date"], right_on=["idest", "date"])
    df = df.set_index(["date"]).sort_index()

    # agrupar por fecha y estacion para sumar los registros de varios recintos en misma estacion y dia
    df = df[["idest", "n_um", "detected"]].groupby(["date", "idest"]).agg("sum")
    df.reset_index(inplace=True)
    df = df.sort_values(["idest", "date"])
    df = df.set_index("date")

    # calcular incidencia acumulada 10 dias
    grouped_sum = df[["idest", "detected", "n_um"]].groupby("idest").rolling('10d').sum()
    grouped_sum.columns = ["idest"] + [x + "_10d" for x in ["detected", "n_um"]]
    grouped_sum = grouped_sum.fillna(0)

    # df_result = df_result.reset_index(0)
    # df_result = df_result.set_index(["idest", "date"])
    #
    # df_merged = df_result.join(grouped_sum.drop("idest", axis=1), how="inner")
    #
    # # eliminar valores con NA (90 lineas de  87.000)
    # df_result = df_result.dropna(axis=0)
    # df_result = df_result.reset_index(0).reset_index(0)
    # df_result.columns = [x.lower() for x in df_result.columns.tolist()]
    #
    # # remove rows with na
    # df_result = df_result.drop(df_result.index[df_result['n_um_10d_sum'].isnull()])
    grouped_sum = grouped_sum.drop("idest", axis=1)

    # añadimos altitud y media de altitud de recintos
    grouped_sum.reset_index(inplace=True)
    df_wst = df_result[["idest", "altitud", "altitud_est"]].groupby(["idest"]).agg(
        {"altitud": "mean", "altitud_est": "max"})
    grouped_sum = grouped_sum.merge(df_wst, on="idest")

    # add daily n_um and detected values
    df.reset_index(inplace=True)
    df = df.set_index(["date", "idest"]).sort_index()
    grouped_sum = grouped_sum.set_index(["date", "idest"]).sort_index()
    grouped_sum = grouped_sum.join(df)
    grouped_sum.reset_index(inplace=True)

    return grouped_sum


def data_cleaning(df):
    df = df.replace([np.inf, -np.inf], np.nan)
    # eliminar columns con nan
    df = df.drop(df.index[df['prec_exp_perc_60d'].isnull()])
    # ver informe roya para detalle de limpieda de datos
    df.loc[(df["date"] == "2017-01-30") & (df["idest"] == 9102), "hum_std"] = 11.95
    df.loc[(df["date"] == "2017-10-07") & (df["idest"] == 34004), "hum_std"] = 19.75
    df.drop(df[df.isnull().any(axis=1)].index, inplace=True)


def prepare_dataset(df, output_name):
    df["perc_detected"] = df["detected"] / df["n_um"]
    df["perc_detected_10d"] = df["detected_10d"] / df["n_um_10d"]
    df["perc_detected"].fillna(0, inplace=True)
    df["perc_detected_10d"].fillna(0, inplace=True)

    # lag variables t-1, t-3 and t-10 for each station
    df["perc_detected_10d_t1"] = df.sort_values(["date"]).groupby(["idest"])["perc_detected_10d"].shift(1)
    df["perc_detected_10d_t3"] = df.sort_values(["date"]).groupby(["idest"])["perc_detected_10d"].shift(3)
    df["perc_detected_10d_t10"] = df.sort_values(["date"]).groupby(["idest"])["perc_detected_10d"].shift(10)

    # extract validation dataset, choosing the same weather stations as in bayesian R model
    df_validation = df[df["idest"].isin(["24008", "49005", "34007", "09002"])]
    df_train_test = df[~df["idest"].isin(["24008", "49005", "34007", "09002"])]

    df_train_test.to_parquet("../resources/dataset/{}_train-test.pck".format(output_name))
    df_validation.to_parquet("../resources/dataset/{}_validation.pck".format(output_name))


def rename_v1_dataset():
    """
    Renames v1 dataset to v2 column names
    :return:
    """
    input_files = ['2015_2019_daily_v1.pck',
                   '2015_2019_daily_v1_train-test.pck',
                   '2015_2019_daily_v1_validation.pck']
    col_mapping = {'prec_acum_30': 'prec_30d',
                   'prec_acum_60': 'prec_60d',
                   'dec_event_tot': 'roya_event_10d',
                   'dec_event_tot_skip': 'roya_event_skip_10d',
                   'perc_dec_event_tot_skip': 'roya_perc_event_skip_10d',
                   'prec_acum_exp_30': 'prec_expected_30d',
                   'prec_acum_exp_60': 'prec_expected_60d',
                   'prec_acum_30_porc': 'prec_exp_perc_30d',
                   'prec_acum_60_porc': 'prec_exp_perc_60d',
                   'prec_acum_30_porc_sgm': 'prec_exp_sgm_30d',
                   'detected_10d_sum': 'detected_10d',
                   'n_um_10d_sum': 'n_um_10d'}

    BASE = '/home/gus/workspaces/wpy/roya/resources/dataset'
    for file in input_files:
        full_path = os.path.join(BASE, file)
        df = pd.read_parquet(full_path)
        df.rename(columns=col_mapping, inplace=True)
        df.to_parquet(full_path)


def drop_duplicates_v2():
    """
    El dataset de clima v2 tiene datos duplicados, los datos del día 13-1-2017 están 31 veces repetidos
    :return:
    """
    input_files = ['2015_2019_daily_v2.pck',
                   '2015_2019_daily_v2_train-test.pck',
                   '2015_2019_daily_v2_validation.pck']

    BASE = '/home/gus/workspaces/wpy/roya/resources/dataset'
    for file in input_files:
        full_path = os.path.join(BASE, file)
        df = pd.read_parquet(full_path)
        df = df.sort_values(["idest", "date"]).drop_duplicates(subset=["idest", "date"], keep="first")
        df.to_parquet(full_path)

def prepare_dataset_2020():
    input_file = '/media/data/climate/inforiego_2014-2020_daily.pkt'
    df_weather = pd.read_parquet(input_file)
    df_weather = df_weather[df_weather.date.dt.year == 2020]
    df_weather = prepare_weather_data(df_weather)
    df_weather.drop(['level_0', 'index'], axis=1, inplace=True)
    df_weather.to_parquet("../../resources/dataset/2020_daily_v2.pck")

if __name__ == '__main__':
    # drop_duplicates_v2()
    # exit(0)

    # exit(0)

    # df_weather = pd.read_csv("../R_analysis/data/weather_data.csv")
    input_file = '/media/data/climate/inforiego_2014-2020_daily.pkt'
    df_weather = pd.read_parquet(input_file)
    df_weather = prepare_weather_data(df_weather)
    df_field_data = prepare_parcel_data()
    # convert idest to string with 0-left padding
    df_field_data["idest"] = df_field_data.idest.astype(str).str.pad(5, "left", "0")

    # df_grouped_data = group_data(df_field_data)
    print(df_field_data.shape)
    # antes de hacer el join, eliminamos los datos de clima de las estaciones de las que no teneoms datos
    wsts = df_field_data.idest.unique()

    # idx = pd.IndexSlice
    # df_weather = df_weather.loc[idx[wsts, :], :]
    df_weather = df_weather[df_weather.idest.isin(wsts)]

    df_weather.set_index(["idest", "date"], inplace=True)
    df_field_data.set_index(["idest", "date"], inplace=True)

    df = df_weather.join(df_field_data)
    df.reset_index(inplace=True)
    df.drop(["index"], axis=1, inplace=True)
    df = df.astype(weather_var_types)
    float32_fields_conv = {c: "float32" for c in df.columns if
                           c not in weather_var_types.keys() and c not in ["idest", "date"]}
    df = df.astype(float32_fields_conv)
    data_cleaning(df)

    df.to_parquet("../resources/dataset/2015_2019_daily_v2.pck")

    prepare_dataset(df, '2015_2019_daily_v2')
