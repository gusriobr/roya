import pandas as pd
import numpy as np
DECADE_WSIZE = 10
EVENT_WSIZE = 8  # 2 records/hour > 4 hours
TIME_STEP = 30  # minutes

def preprocess_data():
    # read excel, sort
    # TEST DATA
    df = pd.read_excel("../resources/roya_datos_campo.xlsx", encoding="UTF-8", sheet_name="datos",
                       parse_dates=['FECHA PERVIVENCIA'], decimal=","
                       )
    df_wstation = pd.read_excel("../resources/roya_datos_campo.xlsx", encoding="UTF-8", sheet_name="estaciones",dtypes = {"IDEST", object} )
    df = df.merge(df_wstation, on="ZV")
    df["IDEST"] = df["IDEST"].astype(str)



    df = df.sort_values(by=["IDEST", "FECHA PERVIVENCIA"])

    # filter and sort columns
    df = df.drop(['PROVINCIA', 'COMARCA', 'SAC'], axis=1)
    col_order = df.columns.values.tolist()
    # move idest to first position
    idest = col_order.pop()
    col_order = [idest] + col_order
    df = df[col_order]

    # rename columnas
    col_order = df.columns.values.tolist()
    new_cols = []
    for c in col_order:
        c = c.replace(" ", "_")
        if "%" in c:
            c = c.replace("%", "PORC_")
        if "º" in c:
            c = c.replace("º", "UM")
        new_cols.append(c)
    df.columns = new_cols

    df.to_hdf("../resources/processed/datos_campo.h5", "data")
    df.to_excel("../resources/processed/datos_campo_out.xlsx", encoding="UTF-8", sheet_name="datos")


if __name__ == '__main__':
    preprocess_data()
