import os
from calendar import monthrange

import numpy as np
import pandas as pd

from data.evaluate_roya import evaluate_daily_events

BASE_FOLDER = "/media/data/climate"

types = {'prov': np.int8, 'idest': np.int8, 'YEAR': int,
         'HORAMIN': int, 'TEMPMEDIA': np.float32,
         'HUMEDADMEDIA': np.float32,
         'VELVIENTO': np.float32, 'DIRVIENTO': np.float32, 'RADIACION': np.float32,
         'PRECIPITACION': np.float32,
         'TEMPMEDIACAJA': np.float32}

RAIN_EXPC_REF_YEAR = 2019

def calculate_daily_roya_events(df):
    """
    :param df: df with hourly weather events
    :return:
    """
    df.sort_values(by=["idest", "date"], inplace=True)
    lst_stations = df["idest"].unique()

    df_daily = None
    total_daily_data = []
    # lst_stations = ["05001"]
    for st in lst_stations:
        # iterate over weather stations calculating events
        df_filtered = df[df["idest"] == st]
        # iterate over weather records to evaluate events on 4-hour windows
        index_list = df_filtered.index.values.tolist()

        # we use df_filtered to get the indexes, but the modification is done on the main dataframe
        daily_data = evaluate_daily_events(st, df, index_list)
        total_daily_data.extend(daily_data)

        if df_daily is None:
            df_daily = pd.DataFrame(daily_data)
        else:
            df_daily = df_daily.append(pd.DataFrame(daily_data))
            df_daily["date"] = pd.to_datetime(df_daily["date"])

    # aggregate roya daily events per decade
    df_daily.set_index("date", inplace=True)
    df_daily.sort_values(["idest", "date"], inplace=True)
    df_grouped = df_daily.groupby("idest").rolling("10D").sum()
    df_grouped.drop(['EVENT_PER', 'EVENT_PER_SKIP'], axis=1, inplace=True)
    df_grouped.rename(columns={'EVENT_TOT_SKIP': 'roya_event_skip_10d', 'EVENT_TOT': 'roya_event_10d'}, inplace=True)

    return df_grouped


def impute_and_resample(df):
    """"
    Create cartesian product with date-range and idest's and merge with the original dataframe to find gaps in the data,
    them impute missing data using interpolation.
    Once the gaps are filled, resample data to 1H scale without aggregation, just getting the data at .00 minute
    """

    # for idest in df.idest.unique():
    #     # get min and max days

    dlimits = df.groupby("idest").agg({"date": ["min", "max"]}).droplevel(0, axis=1).reset_index()

    df_full = None
    for i, r in dlimits.iterrows():
        dranges = pd.DataFrame(pd.date_range(start=r["min"], end=r["max"], freq="30min"), columns=["date"])
        dranges["idest"] = r["idest"]
        if df_full is None:
            df_full = dranges
        else:
            df_full = pd.concat([df_full, dranges])

    df_full = df_full.merge(df, how="left", on=["date", "idest"])
    # impute missing data
    df_full = df_full.groupby('idest').apply(lambda x: x.interpolate(method="linear", limit=6, limit_direction="both"))

    df_full = df_full.set_index(["date"])  # .sort_index()
    df_full = df_full.sort_values(["idest", "date"])
    # resample data to 1H scale without aggregation
    df_full = df_full.groupby(["idest"]).resample("1H").first()
    df_full.drop("idest", axis=1, inplace=True)

    return df_full


def calculate_daily_data(df, debug=False):
    """
    :param df: hourly weather data dataframe
    :param debug:
    :return:
    """
    # input_file = '/media/data/climate/inforiego_2014-2020_hourly.pkt'
    # input_file = '/media/data/climate/inforiego_2014-2020_hourly_test.pkt'
    # df = df[(df["year"] == 2020) & (df["idest"].isin(['40002', '42001']))]

    # calculate rolling values
    df = df.set_index(["date"])  # .sort_index()
    df = df.sort_values(["idest", "date"])
    cols = ['temp', 'prec', 'hum', 'wdir', 'wspeed', 'radiation']
    # cols = ['temp', 'prec']
    metrics = ['min', 'max', 'mean', 'median', 'std']
    df_grouped = df[["idest"] + cols].groupby("idest").rolling('10d').agg(metrics)

    df_grouped.columns = ['_'.join(col).strip() for col in df_grouped.columns.values]
    df_grouped.rename(columns={'date_': 'date', 'idest_': 'idest'}, inplace=True)
    df_grouped.drop([x for x in df_grouped.columns if x.startswith("idest_")], axis=1, inplace=True)
    df_grouped = df_grouped.reset_index(0)

    df_daily = df_grouped.at_time("0:00")
    df_daily.reset_index(0, inplace=True)
    df_daily.set_index(["idest", "date"], inplace=True)

    if debug:
        df_grouped.reset_index(0).sort_values(["idest", "date"]).to_excel(
            "/tmp/salida_metrics.xlsx")

    # rainfall 10d cumm
    for freq in ["10d", "30d", "60d"]:
        df_grouped = df[["idest", "prec"]].groupby("idest").rolling(freq).agg("sum").drop("idest", axis=1)
        df_grouped.rename(columns={'prec': "prec_{}".format(freq)}, inplace=True)
        df_grouped.reset_index(0, inplace=True)
        df_grouped = df_grouped.at_time("0:00")

        df_grouped.reset_index(0, inplace=True)
        df_grouped.set_index(["idest", "date"], inplace=True)

        df_daily = df_daily.join(df_grouped, how="left")
        if debug:
            df_grouped.reset_index(0).sort_values(["idest", "date"]).to_excel(
                "/tmp/salida_{}.xlsx".format(freq))

    # 4-30 thermal integral
    df_grouped = df.groupby("idest").rolling("10D").agg({"temp": thermal_integral})
    df_grouped.rename(columns={'temp': "temp_int_4_30"}, inplace=True)
    df_grouped.reset_index(0, inplace=True)
    df_grouped = df_grouped.at_time("0:00")
    df_grouped.reset_index(0, inplace=True)
    df_grouped.set_index(["idest", "date"], inplace=True)
    # join to daily data
    df_daily = df_daily.join(df_grouped, how="left")

    if debug:
        df_grouped.reset_index(0).sort_values(["idest", "date"]).to_excel(
            "/tmp/salida_int_term.xlsx")

    cols = ['temp_min', 'temp_max', 'temp_mean', 'temp_median',
            'temp_std', 'prec_min', 'prec_max', 'prec_mean',
            'prec_median', 'prec_std', 'hum_min', 'hum_max', 'hum_mean',
            'hum_median', 'hum_std', 'wdir_min', 'wdir_max', 'wdir_mean',
            'wdir_median', 'wdir_std', 'wspeed_min', 'wspeed_max', 'wspeed_mean',
            'wspeed_median', 'wspeed_std', 'radiation_min', 'radiation_max',
            'radiation_mean', 'radiation_median', 'radiation_std', 'prec_10d',
            'prec_30d', 'prec_60d', 'temp_int_4_30']
    df_daily[cols] = df_daily[cols].astype(np.float32)

    if debug:
        df_daily.reset_index(0).to_excel(os.path.join(BASE_FOLDER, "inforiego_2014-2020_daily_test.xlsx"))

    return df_daily


def thermal_integral(x, days=10):
    last_day = x.index[-1]
    cutoff_date = last_day - pd.Timedelta(days=days)
    value = x[(x.index > cutoff_date) & (x.between(4, 30))].sum()
    # print(value)
    return value


def expected_rainfall_closure(idest, days):
    def def_function(x):
        last_day = x.index[-1]
        try:
            last_day = last_day.replace(year=RAIN_EXPC_REF_YEAR)  # DF_EXPECTED_RAINFALL is calculated using 2020 months
        except:
            last_day = last_day.replace(year=RAIN_EXPC_REF_YEAR,
                                        day=last_day.day - 1)  # DF_EXPECTED_RAINFALL is calculated using 2020 months
        cutoff_date = last_day - pd.Timedelta(days=days)
        dfr = DF_EXPECTED_RAINFALL
        value = dfr[(dfr.idest == idest) & (dfr.date.between(cutoff_date, last_day, inclusive=True))]["rainfall"].sum()
        return pd.Series(value)

    return def_function


def prepare_expected_railfall_df():
    input_file = os.path.join("../../", 'resources/weather/datos_estaciones.csv')
    df_st = pd.read_csv(input_file, sep=";")
    df_result = None
    for index, row in df_st.iterrows():
        month = 1
        for col in range(7, 19):
            col_name = df_st.columns[col]
            total_rainfall = row[col_name]
            # take one month without 29-day-feb
            num_days = monthrange(RAIN_EXPC_REF_YEAR, month)[1]
            rain_per_day = total_rainfall / num_days
            # add two years so we can calculate 30/60 backwards for january/febrary
            df_month = pd.concat([pd.DataFrame(
                pd.date_range(start='{}/{}/1'.format(RAIN_EXPC_REF_YEAR - 1, month),
                              end='{}/{}/{}'.format(RAIN_EXPC_REF_YEAR - 1, month, num_days), freq="1D")),
                pd.DataFrame(pd.date_range(start='{}/{}/1'.format(RAIN_EXPC_REF_YEAR, month),
                                           end='{}/{}/{}'.format(RAIN_EXPC_REF_YEAR, month, num_days),
                                           freq="1D"))])
            df_month.rename(columns={0: 'date'}, inplace=True)
            df_month["idest"] = row["idest"]
            df_month["rainfall"] = rain_per_day
            if df_result is None:
                df_result = df_month
            else:
                df_result = pd.concat([df_result, df_month])
            month += 1

    df_result = df_result.sort_values(["idest", "date"])
    df_result["idest"] = df_result["idest"].astype(str).str.pad(5, "left", fillchar="0")
    return df_result


def sigmoid(X, c=1):
    return 1 / (1 + np.exp(-1 * c * X))


def calculate_expected_rainfall_features(df):
    """
    Calculates the expected rainfall according to 30-year series for the last 30-60 days.
    :param df: main dataframe, it MUST BE date-indexed
    :return:
    """
    df = df.reset_index(0)  # set date index
    if not isinstance(df.index, pd.DatetimeIndex):
        df.set_index(["date"], inplace=True)
    df.sort_index()
    wstations = df.idest.unique()
    for st in wstations:
        for num_days in [30, 60]:
            function = expected_rainfall_closure(st, num_days)
            # idest column is  used to pass just one column but it is not used
            df.loc[df.idest == st, "prec_expected_{}d".format(num_days)] = df[df.idest == st]["idest"] \
                .rolling("1D").apply(function, raw=False)

    return df


def calculate_expected_prec_features(df):
    df['prec_exp_perc_30d'] = (df["prec_30d"] - df["prec_expected_30d"]) / df["prec_expected_30d"]
    df['prec_exp_perc_60d'] = (df["prec_60d"] - df["prec_expected_60d"]) / df["prec_expected_60d"]

    df['prec_exp_sgm_30d'] = sigmoid(df['prec_exp_perc_30d'])
    df['prec_exp_sgm_60d'] = sigmoid(df['prec_exp_perc_60d'])

    return df


DF_EXPECTED_RAINFALL = prepare_expected_railfall_df()

if __name__ == '__main__':
    ###########################
    # Test input and resample
    ###########################
    # input = '/media/data/climate/inforiego_2014-2020_hourly.pkt'
    # df_hourly = pd.read_parquet(input)
    #
    # df_hourly = impute_and_resample(df_hourly)
    # df_hourly.to_parquet('/media/data/climate/inforiego_2014-2020_hourly2.pkt')
    #
    # exit()
    pass
