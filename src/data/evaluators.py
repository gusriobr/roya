import calendar
import logging

import numpy as np
import pandas as pd


def fullfill_reqs(row):
    return row["hum"] >= 92 \
           and row["temp"] >= 4 and row["temp"] <= 12 \
           and row["rainfall"] < 0.1;


class WeatherEventEvaluator:

    def __init__(self):
        self.TIME_STEP = 30  # minutes
        self.last_row = None
        self.reset()
        self.reset_skip()
        self.num_times_event_skipped = 0  # number of times the event check has been skipt
        self.num_half_hours_with_skip = 0
        self.num_half_hours = 0
        self.pending_events = 0
        self.min_events_to_skip = 4

    def reset(self):
        self.num_half_hours = 0

    def reset_skip(self):
        self.num_half_hours_with_skip = 0

    def validate_time_step(self, last_date, current_date):
        delta = (current_date - last_date).seconds / 60
        if round(delta) > self.TIME_STEP:
            logging.info("Missing data, {} minutes skipped in {}.".format(delta, self.last_row.values))
            return False
        else:
            return True

    def add_data(self, row, date):
        # make sure the difference in time between current value and the
        # previous one is 30 minutes, if there are missing data, stop evaluating preceding data
        if self.last_row is not None:
            if not self.validate_time_step(self.last_row["date"], row["date"]):
                self.reset()
                self.reset_skip()
                self.pending_events = 0

        if fullfill_reqs(row):
            self.num_half_hours += 1
            if self.pending_events > 0:  # count pending events from last period
                self.num_half_hours_with_skip = self.pending_events
                self.pending_events = 0
            self.num_half_hours_with_skip += 1
        else:
            # reset counters
            if self.num_half_hours >= self.min_events_to_skip:
                # there's an active sequence of events with at least min_events_to_skip length and no
                # skips has been taken yet
                # if the requirements are not fulfilled, we can continue counting if we have found
                # at most 1 missing event, but only if the error record is not the first.
                self.pending_events = self.num_half_hours_with_skip + 1
            else:
                self.pending_events = 0

            self.reset()
            self.reset_skip()

        self.last_row = row

    def evaluate(self):
        row_map = {"EVENT": self.num_half_hours, "EVENT_SKIP": self.num_half_hours_with_skip}
        return row_map


class DayEvaluator:
    """
    Class that stores the state relate to cumulative event counting on a day basis
    """

    def __init__(self, num_consecutive_events=8):
        self.events = []
        self.events_skip = []
        self.last_date = None
        self.MIN_CONSEC_EVENTS = num_consecutive_events

    def reset(self):
        """
        initialize counters
        :return:
        """
        self.events = []
        self.events_skip = []

    def add_data(self, row, date):
        # def add_data(self, event_count, event_skip_count, date):
        """
        Adds data for current day
        :return:
        """
        event_count = row["EVENT"]
        event_skip_count = row["EVENT_SKIP"]

        if self.last_date != date:
            # new day
            self.last_date = date
            self.next_day_records = row
        else:
            self.events.append(event_count)
            self.events_skip.append(event_skip_count)

    def evaluate(self):
        """
        Evaluates ranges for current day
        :return:
        """
        num_half_hours_day = 1
        event_per = 0
        event_per_skip = 0
        event_tot = 0
        event_skip_tot = 0
        last_event_value = -1
        last_event_skip_value = -1
        current_events = 0
        current_events_skip = 0
        for idx, r in enumerate(self.events):
            current_events = r
            current_events_skip = self.events_skip[idx]

            # to count one valid period, the last half-hour record must have a > MIN_CONSEC_EVENTS value and
            # current EVENT must be 0, so the last period has finished and we have to register it.
            # Register it just if current event is not the first in the day, so we don't duplicate periods
            # that expands over the 0:00
            if current_events == 0 and last_event_value >= self.MIN_CONSEC_EVENTS:
                event_per += 1
                # the event could have started in the previous day, count only the time wrt current day
                event_tot += min(last_event_value, num_half_hours_day - 1)
            if current_events_skip == 0 and last_event_skip_value >= self.MIN_CONSEC_EVENTS:
                event_per_skip += 1
                event_skip_tot += min(last_event_skip_value, num_half_hours_day - 1)

            last_event_value = current_events
            last_event_skip_value = current_events_skip
            num_half_hours_day += 1

        # when the day finishes, and there's and event ongoing, register it
        if current_events >= self.MIN_CONSEC_EVENTS:
            event_per += 1
            # the event could have started in the previous day, count only the time wrt current day
            event_tot += min(last_event_value, num_half_hours_day - 1)
        if current_events_skip >= self.MIN_CONSEC_EVENTS:
            event_per_skip += 1
            event_skip_tot += min(last_event_skip_value, num_half_hours_day - 1)

        row = {"EVENT_PER": event_per, "EVENT_PER_SKIP": event_per_skip,
               "EVENT_TOT": event_tot, "EVENT_TOT_SKIP": event_skip_tot}
        self.reset()

        # recover next day events
        self.add_data(self.next_day_records, self.last_date)

        return row


class DecadeEvaluator:
    """
    Class that stores the state related to day decades to register grouped weather events
    """

    def __init__(self, decade_size=10):
        # store number of elements
        self.decade = []
        self.decade_size = decade_size
        pass

    def reset(self):
        """
        initialize counters
        :return:
        """
        pass

    def add_data(self, row, date):
        # remove first element and store current row values
        if len(self.decade) > 0:
            if len(self.decade) == self.decade_size:
                self.decade.pop(0)
        self.decade.append(row)

    def evaluate(self):
        # go over current row and previous days evaluating number of events
        # IDEST, DATE, EVENT_PER, EVENT_PER_SKIP, EVENT_TOT, EVENT_TOT_SKIP,
        indic = {
            "DEC_EVENT_PER": 0,
            "DEC_EVENT_PER_SKIP": 0,
            "DEC_EVENT_TOT": 0,
            "DEC_EVENT_TOT_SKIP": 0
        }
        for day in self.decade:
            indic["DEC_EVENT_PER"] = indic["DEC_EVENT_PER"] + day["EVENT_PER"]
            indic["DEC_EVENT_PER_SKIP"] = indic["DEC_EVENT_PER_SKIP"] + day["EVENT_PER_SKIP"]
            indic["DEC_EVENT_TOT"] = indic["DEC_EVENT_TOT"] + day["EVENT_TOT"]
            indic["DEC_EVENT_TOT_SKIP"] = indic["DEC_EVENT_TOT_SKIP"] + day["EVENT_TOT_SKIP"]
        return indic


class WeatherHistogram:
    def __init__(self, range=10, infix=""):
        # stores current day records
        self.last_date = None
        self.next_day_records = None
        self.range = range
        self.keys = [["TEMP", "TEMPMEDIA"],
                     ["HUMEDAD", "HUMEDADMEDIA"],
                     ["PRECIPITACION", "PRECIPITACION"]]
        # A map that stores per each magnitude the values observed along the range
        # per each magnitude it stores one value per time-interval, these values are
        # added when the method evaluate() is called
        self.cummulative = {}
        # A map that stores per each magnitude the values observed in current day
        # per each magnitude it stores one value per time-interval
        self.current_day = {}
        self._init_cumm_array()
        self.infix = infix
        if self.infix:
            self.infix = "_" + self.infix

    def _init_cumm_array(self):
        for k, v in self.keys:
            self.cummulative[k] = []
            self.current_day[k] = []

    def reset(self):
        """
        initialize counters
        :return:
        """
        self._init_cumm_array()

    def add_data(self, row, date):
        """
        Adds data for current day
        :return:
        """
        if self.last_date != date:
            # new day
            self.last_date = date
            self.next_day_records = row
        else:
            for k, v in self.keys:
                self.current_day[k].append(row[v])

    def _add_cummulative_data(self, current_day):
        if len(self.cummulative) != 0:
            # If the number of accumulated observations has exceed the range,
            # remove the first one
            if len(self.cummulative[self.keys[0][0]]) > self.range:
                for k, v in self.keys:
                    self.cummulative[k].pop(0)

        for k, v in self.keys:
            if len(current_day) > 0:
                self.cummulative[k].append(current_day[k])

    def evaluate(self):
        """
        Extract weather magnitudes distribution indicators.
        :return:
        """
        # save cummulative data
        self._add_cummulative_data(self.current_day)

        # create a list per each measure will data from all days
        map = {}
        for k, v in self.keys:
            array = np.array(self.cummulative[k])
            if array.shape[1] == 0:  # no previous data
                array = np.zeros(1)
            map[k + "{}_MIN".format(self.infix)] = array.min()
            map[k + "{}_MEAN".format(self.infix)] = array.mean()
            map[k + "{}_MEDIAN".format(self.infix)] = np.median(array)
            map[k + "{}_MAX".format(self.infix)] = array.max()
            map[k + "{}_STD".format(self.infix)] = array.std()

        self.reset()
        # recover next day events
        self.add_data(self.next_day_records, self.last_date)

        return map


class DailyAccumulationEvaluator:
    """
    Class that accumulates last 30d rainfall
    """

    def __init__(self, range=30, reference_field="PRECIPITACION", field=None):
        # stores current day records
        self.range = range
        self.reference_field = reference_field
        self.current_day_records = []
        self.cumulative_rain = []

        self.last_date = None
        self.next_day_records = None
        if not field:
            self.field = "{}_{}_{}".format(self.reference_field[:4], "ACUM", self.range)

    def reset(self):
        """
        initialize counters
        :return:
        """
        self.current_day_records = []
        # self.cumulative_rain = []

    def add_data(self, row, date):
        """
        Adds data for current day
        :return:
        """
        if self.last_date != date:
            # new period
            self.last_date = date
            self.next_day_records = row
        else:
            self.current_day_records.append(row[self.reference_field])

    def _check_day_range(self, current_day_records):
        """
        Updates the list of daily-data used to calculate the cumulative data
        :param current_day_records:
        :return:
        """
        if len(self.cumulative_rain) > self.range:
            self.cumulative_rain.pop(0)
        self.cumulative_rain.append(current_day_records)

    def evaluate(self):
        """
        Extract weather magnitudes distribution indicators.
        :return:
        """
        # save cummulative data
        self._check_day_range(self.current_day_records)

        # sum all list to get a unique list
        full_list = sum(self.cumulative_rain, [])
        rain_arr = np.array(full_list)
        row_map = {self.field: np.sum(rain_arr)}
        self.reset()
        # recover next day events
        self.add_data(self.next_day_records, self.last_date)

        return row_map


class ValueIntegralEvaluator:
    """
    Accumulates a magnitude when its value is within and expected range
    """

    def __init__(self, min, max, reference_fields):
        """
        constructor
        :param range: number of days used to evaluate
        :param reference_field:
        :patam reference_fields: fields read from the row use to accumulate data.
        :param field:
        """
        self.min = min
        self.max = max
        self.keys = reference_fields

        # stores current day records
        self.last_date = None
        self.next_day_records = None
        # A map that stores per each magnitude the accumulated values observed along the range.
        # Per each magnitude it stores the integral value observed so far
        self.cumulative = {}
        # A map that stores per each magnitude the number of times that the condition of the integral has been commited
        self.num_events = {}
        self._init_cumm_maps()

    def _init_cumm_maps(self):
        for k in self.keys:
            self.cumulative[k] = 0
            self.num_events[k] = 0

    def reset(self):
        """
        If next day is the first day of year, initialize counters.
        :return:
        """
        if self.last_date is None or (self.last_date.month == 1 and self.last_date.day == 1):
            self._init_cumm_maps()

    def add_data(self, row, date):
        """
        Adds a one-record data for current day
        :return:
        """
        if self.last_date != date:
            # new day
            self.last_date = date
            self.next_day_records = row
        else:
            for k in self.keys:
                v = row[k]
                if self.min <= v <= self.max:
                    self.cumulative[k] += v
                    self.num_events[k] += 1

    def _check_day_range(self):
        """
        Updates the list of daily-data used to calculate the cumulative data
        :param current_day_records:
        :return:
        """
        pass  # do nothing

    def evaluate(self):
        """
        Extract weather magnitudes distribution indicators.
        :return:
        """
        # save cummulative data
        self._check_day_range()

        # create a list per each measure will data from all days
        map = {}
        for k in self.keys:
            fname = "{}_{}_{}_{}".format(k, "INTEG", self.min, self.max)
            # data is registered half-hour, divide by two to transform in hour-based events
            map[fname] = self.cumulative[k] / 2.0
            fname = "{}_{}_{}_{}".format(k, "INTEG_EVTS", self.min, self.max)
            map[fname] = self.num_events[k] / 2

        self.reset()

        # recover next day events
        self.add_data(self.next_day_records, self.last_date)

        return map


class CummulativeClimateMeanRainFall:
    """
    Calculates the averaged rain expected according to climate data.
    """

    def __init__(self, df_station, range=30, field=None):
        # stores current day records
        self.range = range
        self.current_day = None
        self.last_date = None
        self.next_day_records = None
        if not field:
            field = "PREC_ACUM_EXP_" + str(self.range)
        self.field = field
        self.df_station = df_station

        self.current_station = None
        self.cummulative_rain = []

    def reset(self):
        """
        initialize counters
        :return:
        """
        self.current_day = None

    def add_data(self, row, date):
        """
        Adds data for current day
        :return:
        """
        self.current_day = date
        self.current_station = row["IDEST"]

    def _calc_expected_value(self, f_ini, f_end, station_id):
        # get months
        m_ini = f_ini.month
        m_end = f_end.month
        index_month = m_ini
        diff_years = f_end.year - f_ini.year
        precs = 0
        while index_month <= m_end + (12 * diff_years):
            current_month = (index_month % 12)
            if current_month == 0:
                current_month = 12
            if current_month == m_ini:
                # if this is the first month, get the days till months's end
                if m_end == m_ini:
                    # the date range starts and ends in the same month
                    num_days = f_end.day - f_ini.day
                else:
                    month_days = calendar.monthrange(f_ini.year, f_ini.month)[1]
                    num_days = month_days - f_ini.day + 1
                precs += self._get_prec(station_id, f_ini.year, current_month, num_days)
            elif current_month == m_end:
                # last month, get offset days
                num_days = f_end.day
                precs += self._get_prec(station_id, f_end.year, current_month, num_days)
            else:
                # get the whole month. February and leap-year not taken in account
                num_days = calendar.monthrange(f_ini.year, current_month)[1]
                precs += self._get_prec(station_id, f_ini.year, current_month, num_days)
            index_month += 1

        return precs

    def _get_prec(self, station_id, year, month, num_days):
        month_key = "prec_" + str(month).zfill(2)
        prec_expected_month = self.df_station[self.df_station.idest == station_id][month_key].iloc[0]
        month_days = calendar.monthrange(year, month)[1]
        return prec_expected_month * (num_days / month_days)

    def _add_rain_day(self, current_day):
        if len(self.cummulative_rain) > self.range:
            self.cummulative_rain.pop(0)
        self.cummulative_rain.append(current_day)

    def evaluate(self):
        """
        Extract weather magnitudes distribution indicators.
        :return:
        """
        if self.current_day is None:
            return {self.field: 0.0}

        # save cummulative data
        self._add_rain_day(self.current_day)

        # get first and last day
        f_ini = self.cummulative_rain[0]
        f_end = self.cummulative_rain[-1]
        prec = self._calc_expected_value(f_ini, f_end, self.current_station)

        row_map = {self.field: prec}
        self.reset()

        return row_map


class EvaluatorsFactory:
    RECORD_UPDATERS = []
    HOURLY_EVALUATORS = []
    DAILY_EVALUATORS = []
    station_df = None

    def __init__(self):
        EvaluatorsFactory.station_df = self._load_stations_data()
        EvaluatorsFactory.reset()

    @staticmethod
    def reset():
        if EvaluatorsFactory.station_df is None:
            EvaluatorsFactory.station_df = EvaluatorsFactory._load_stations_data()

        EvaluatorsFactory.RECORD_UPDATERS = [
            WeatherEventEvaluator(),
        ]
        EvaluatorsFactory.HOURLY_EVALUATORS = [
            DayEvaluator(),
            # WeatherHistogram(range=10),
            # DailyAccumulationEvaluator(range=30, reference_field="PRECIPITACION"),
            # DailyAccumulationEvaluator(range=60, reference_field="PRECIPITACION"),
            # ValueIntegralEvaluator(min=4, max=30, reference_fields=["TEMPMEDIA"])
        ]
        EvaluatorsFactory.DAILY_EVALUATORS = [
            # DecadeEvaluator(),
            # CummulativeClimateMeanRainFall(EvaluatorsFactory.station_df, range=30),
            # CummulativeClimateMeanRainFall(EvaluatorsFactory.station_df, range=60)
        ]

    @staticmethod
    def _load_stations_data():
        # TODO: file name should be obtained from configuration object
        input_file = "../../resources/weather/datos_estaciones.csv"
        prec_fields = ["prec_" + str(x).zfill(2) for x in range(1, 13)]
        dtypes = {k: np.float16 for k in prec_fields}
        dtypes["idest"] = str

        df_wstations = pd.read_csv(input_file, sep=";", encoding="UTF-8", decimal=".", dtype=dtypes)
        return df_wstations

    @staticmethod
    def get_record_updaters():
        return EvaluatorsFactory.RECORD_UPDATERS

    @staticmethod
    def get_hourly_evaluators():
        return EvaluatorsFactory.HOURLY_EVALUATORS

    @staticmethod
    def get_daily_evaluators():
        return EvaluatorsFactory.DAILY_EVALUATORS
