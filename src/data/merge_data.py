import pandas as pd
import numpy as np

if __name__ == '__main__':
    weather_file ="../resources/processed/datos_clima_desde_2015_daily.h5"
    ground_file ="../resources/processed/datos_campo.h5"

    df_ground = pd.read_hdf(ground_file, "data")
    df_weather = pd.read_hdf(weather_file, "data")

    df = df_ground.merge(df_weather, left_on=["IDEST","FECHA_PERVIVENCIA"], right_on=["IDEST","DATE"])
    df = df.drop(['FECHA_PERVIVENCIA'], axis=1)


    df.to_hdf("../resources/processed/full_data.h5", "data")