select /*csv*/ IDPROVINCIA,IDESTACION,AÑO AS ANO,DIA,FECHA,FECHACOMPLETA,HORAMIN,TEMPMEDIA,HUMEDADMEDIA,VELVIENTO,DIRVIENTO,RADIACION,PRECIPITACION,TEMPMEDIACAJA
from ita_inforiego.datoshorarios
where lpad(idprovincia,2,'0')||lpad(idestacion,3,'0') in ('05001','05101','37101','47002','09004','09007','09005','34008','34006','34001',
'09102','42002','09101','09003','47005','09002','34007','24005','24009','24008','24002','47001','24003','24007','24006','34003','47101',
'34101','47104','34004','34002','37102','37003','37001','40001','40002','42001','50015','42101','42003','47003','47007','47006','49004',
'47102','47008','47103','49008','49005','49002','49006','49001')
and año >=2015;