--------------------------------
--- EJEMPLO DE CREACIÓN DE CAPA GEOGRÁFICA CON SCRIPTS
--------------------------------
-- Crear una capa:
CREATE TABLE geometry_test (ID smallint unique, shape sde.st_geometry);
-- Rellear con datos del SIGPAC
insert into geometry_test (id, shape) select objectid, shape from ITA_COMUN.RECFE14_140202 where objectid between 10 and 20;
commit;
--  Crear índice espacial:
CREATE INDEX geotest_idx ON geometry_test(shape) INDEXTYPE IS sde.st_spatial_index  PARAMETERS('st_grids=100 st_srid=25830');

-- tabla temporal para registrar las inspecciones y obtener lista de recintos únicos
CREATE TABLE temp_inspeccion (
	REFREC NVARCHAR2(23),
	ANIO NUMBER(4,0),
	PROVINCIA NUMBER(5, 0) NOT NULL ,
	MUNICIPIO NUMBER(5, 0) NOT NULL ,
	AGREGADO NUMBER(5, 0) NOT NULL ,
	ZONA NUMBER(5, 0) NOT NULL ,
	POLIGONO NUMBER(5, 0) NOT NULL ,
	PARCELA NUMBER(10, 0) NOT NULL ,
	RECINTO NUMBER(10, 0) NOT NULL ,
	USO_SIGPAC NVARCHAR2(2) 
);


--  tabla con recintos unicos
CREATE TABLE RECINTO_ROYA (
	REFREC NVARCHAR2(23),
	ANIO NUMBER(4,0),
	PROVINCIA NUMBER(5, 0) NOT NULL ,
	MUNICIPIO NUMBER(5, 0) NOT NULL ,
	AGREGADO NUMBER(5, 0) NOT NULL ,
	ZONA NUMBER(5, 0) NOT NULL ,
	POLIGONO NUMBER(5, 0) NOT NULL ,
	PARCELA NUMBER(10, 0) NOT NULL ,
	RECINTO NUMBER(10, 0) NOT NULL ,
	USO_SIGPAC NVARCHAR2(2) ,
	recinto_shape sde.st_geometry,
	centroide sde.st_geometry,
	ALTITUD NUMBER(10,3),
	IDEST VARCHAR(10),
	DIST_IDEST NUMBER(10,3),
	ALTITUD_EST NUMBER(10,3),
	IDEST_100 VARCHAR(10),
	DIST_IDEST_100 NUMBER(10,3),
	ALTITUD_EST_100 NUMBER(10,3)
);

CREATE INDEX A77_IX1 ON RECINTO_ROYA (recinto_shape) INDEXTYPE IS SDE.ST_SPATIAL_INDEX 
PARAMETERS('ST_GRIDS = 660 ST_SRID = 300002 ST_COMMIT_ROWS = 10000  PCTFREE 0 INITRANS 4') ;

CREATE INDEX A77_IX2 ON RECINTO_ROYA (centroide) INDEXTYPE IS SDE.ST_SPATIAL_INDEX 
PARAMETERS('ST_GRIDS = 660 ST_SRID = 300002 ST_COMMIT_ROWS = 10000  PCTFREE 0 INITRANS 4') ;


insert into recinto_roya (REFREC,PROVINCIA,MUNICIPIO,AGREGADO,ZONA,POLIGONO,PARCELA,RECINTO,USO_SIGPAC, recinto_shape, centroide)
select c_REFREC,c_PROVINCIA,c_MUNICIPIO,c_AGREGADO,c_ZONA,c_POLIGONO,c_PARCELA,c_RECINTO,c_USO_SIGPAC, shape, sde.st_centroid(shape)
from ita_comun.recfe17_170105 r, (select distinct refrec from temp_inspeccion) i
where r.c_refrec = i.refrec;

-- EN QGIS OBTENER LAS ALTITUDES DE RECINTO Y CARGARLAS EN LA TABLA RECINTO_ROYA
CREATE TABLE ALTITUDES_RECINTO (
	REFREC VARCHAR2(23),
	ALTITUD NUMBER(18,4)
);
--- CARGAR  RECINTOS Y ACTUALIZAR ALTITUDES
UPDATE RECINTO_ROYA R SET ALTITUD = (SELECT ALTITUD/1000000 FROM ALTITUDES_RECINTO A WHERE R.REFREC = A.REFREC)


-- crear tabla temporal con las distancias entre recintos y estaciones
create table tmp_distances as
select LPAD(idprovincia, 2, '0')||LPAD(idestacion, 3, '0') idest, e.sestacion, e.altitud, r.altitud as altitud_rec, r.refrec, sde.st_distance(centroide, e.shape) distancia
from ITA_INFORIEGO.ESTACIONES_EXTENDIDA e, recinto_roya r

-- obtenemos el identificador, altitud y distancia a la estación más cercana
-- crear tabla temporal, la consulta tarda demasiado
create table tmp_min_distances as
select *
    FROM (
    SELECT IDEST, SESTACION, refrec, ALTITUD,altitud_red,DISTANCIA, MIN(DISTANCIA)  OVER (PARTITION BY refrec) AS MIN_DIST
    FROM tmp_distances t
    ) t
    WHERE DISTANCIA=MIN_DIST;
    
update recinto_roya r set (IDEST,DIST_IDEST,ALTITUD_EST) = (select idest, distancia,altitud
    FROM tmp_min_distances t
WHERE DISTANCIA=MIN_DIST and r.refrec = t.refrec);
drop table tmp_min_distances;

-- creamos una segunda tabla con estaciones a < 100m de diferencia de altitud
create table tmp_min_distances_100 as
select *
    FROM (
    SELECT IDEST, SESTACION, refrec, ALTITUD,altitud_red,DISTANCIA, MIN(DISTANCIA)  OVER (PARTITION BY refrec) AS MIN_DIST, abs(altitud- altitud_red) diff
    FROM tmp_distances t
    where abs(altitud - altitud_red)< 100
    ) t
    WHERE DISTANCIA=MIN_DIST;    

update recinto_roya r set (IDEST_100,DIST_IDEST_100,ALTITUD_EST_100) = (select idest, distancia,altitud
    FROM tmp_min_distances_100 t
WHERE DISTANCIA=MIN_DIST and r.refrec = t.refrec);


drop table tmp_min_distances;
drop table tmp_min_distances_100;

-- extract

select /*csv*/ REFREC,ANIO,PROVINCIA,MUNICIPIO,AGREGADO,ZONA,POLIGONO,PARCELA,RECINTO,USO_SIGPAC,recinto_shape,centroide,
ALTITUD,IDEST,DIST_IDEST,ALTITUD_EST,IDEST_100,DIST_IDEST_100,ALTITUD_EST_100,
from recinto_roya;