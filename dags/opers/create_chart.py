import argparse


def create_chart(*args, **kwargs):
    import seaborn as sns

    print(args)
    print(kwargs)
    date = kwargs["start_date"]

    df = sns.load_dataset('iris')
    sns_plot = sns.pairplot(df, hue='species', height=2.5)
    sns_plot.savefig("/tmp/output_{}.png".format(date))


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument("--start", help="Give starting date.")
    args = parser.parse_args()
    date = args.start

    if not date:
        raise Exception("Starting date must be passed as parameter!.")

    create_chart(date)
