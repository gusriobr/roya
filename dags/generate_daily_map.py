import os
from datetime import datetime

from airflow import DAG

from operators.python_operators import PythonVirtualEnvCacheOperator


def wrapper_function(*args, **kwargs):
    from reporting.create_map import create_daily_map
    date = kwargs["date"]
    output_folder = kwargs["DIR_PUBLISH"]

    dts_file = os.path.join(kwargs['DIR_DATALAKE'], 'climate/inforiego_2014-2020_daily.pkt')
    create_daily_map(dts_file, date, output_folder)
    # from opers.create_chart import create_chart
    # return create_chart(*args, **kwargs)


default_args = {
    'start_date': datetime(2020, 5, 1),
    'end_date': datetime(2020, 5, 30),
    'schedule_interval': '@daily',
    'owner': 'airflow'
}

file_path = os.path.dirname(os.path.abspath(__file__))
BASE_FOLDER = os.path.realpath(os.path.join(file_path, "../"))

# , schedule_interval=None
with DAG("generate_daily_map", default_args=default_args) as dag:
    virtualenv_task = PythonVirtualEnvCacheOperator(
        task_id="virtualenv_python",
        venv_path='/home/gus/workspaces/wpy/venvs/mathor',
        sys_path=[os.path.join(BASE_FOLDER, 'dags'),
                  os.path.join(BASE_FOLDER, 'src')],
        python_callable=wrapper_function,
        system_site_packages=False,
        op_kwargs={"date": "{{ds}}"},
        dag=dag,
    )

if __name__ == '__main__':
    from airflow.utils.state import State

    dag.clear(dag_run_state=State.NONE)
    dag.run()
